# sets some additional helpful variables for RBFX sdk

set(RBFX_BUILD_TYPES "RELEASE;DEBUG;RELWITHDEBINFO")

if (DEFINED RBFX_SUBDIR)
  # TODO: refactor from RbfxMinimalCachePreset.txt
  set(ASSIMP_BUILD_ALL_IMPORTERS_BY_DEFAULT "OFF")
  set(URHO3D_GLOW "OFF")
  set(URHO3D_NETWORK "OFF")
  set(URHO3D_PHYSICS "OFF")
  set(URHO3D_PHYSICS2D "OFF")
  set(URHO3D_WEBP "OFF")
  set(URHO3D_URHO2D "OFF")
  set(URHO3D_IK "OFF")
  set(URHO3D_NAVIGATION "OFF")
  set(URHO3D_EXTRAS "OFF")
  set(URHO3D_PROFILING "OFF")
  set(URHO3D_TOOLS "OFF")
  set(URHO3D_EDITOR "OFF")
  set(URHO3D_PLAYER "OFF")
  set(URHO3D_SAMPLES "ON")
  set(URHO3D_GRAPHICS_API "OpenGL")
  add_subdirectory(${RBFX_SUBDIR} rbfx)
else ()
  if (NOT RBFX_SDK_PATH)
    message(FATAL_ERROR "RBFX_SDK_PATH or RBFX_SUBDIR must be set!")
  endif ()
endif()


if (DEFINED RBFX_SDK_PATH)
  if (NOT EXISTS "${RBFX_SDK_PATH}/share/CMake/Urho3D.cmake")
    message (FATAL_ERROR "RBFX_SDK_PATH must point to where share/CMake/Urho3D.cmake is  located (was ${RBFX_SDK_PATH})")
  endif ()

  include ("${RBFX_SDK_PATH}/share/CMake/Urho3D.cmake")

  get_property(RBFX_BUILD_TYPES TARGET Urho3D PROPERTY IMPORTED_CONFIGURATIONS)
  message("Uhro3D imported configs: ${RBFX_BUILD_TYPES}")
endif ()

cmake_policy(SET CMP0057 NEW)

function (add_dll_symlink_target)
  set(options "")
  set(oneValueArgs RBFX_DLL_PATH CONFIGURATION_TYPE)
  set(multiValueArgs "")
  cmake_parse_arguments(ADD_TGT "${options}" "${oneValueArgs}"
    "${multiValueArgs}" ${ARGN} )

  add_custom_target(Symlink_Urho3D_dll_${ADD_TGT_CONFIGURATION_TYPE}
    ALL
    ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/${ADD_TGT_CONFIGURATION_TYPE}
    COMMAND ${CMAKE_COMMAND} -E create_symlink  ${ADD_TGT_RBFX_DLL_PATH} ${CMAKE_BINARY_DIR}/${ADD_TGT_CONFIGURATION_TYPE}/Urho3D.dll
    BYPRODUCTS ${CMAKE_BINARY_DIR}/${ADD_TGT_CONFIGURATION_TYPE}/Urho3D.dll
    )

endfunction ()


function(bring_urho_dll_in)
  set(options "")
  set(oneValueArgs CONFIGURATION_TYPE)
  set(multiValueArgs "")

  set(URHO_${PROCESS_DLL_CONFIGURATION_TYPE}_LIB_PATH "${PROCESS_DLL_CONFIGURATION_TYPE}/Urho3D.dll")
  if (MinGW)
    set(URHO_DEBUG_LIB_PATH "libUrho3D.dll") #TODO different for debug/release
  endif()

  cmake_parse_arguments(PROCESS_DLL "${options}" "${oneValueArgs}"
    "${multiValueArgs}" ${ARGN} )

  string(TOUPPER ${PROCESS_DLL_CONFIGURATION_TYPE} CONFIGURATION_TYPE_CAPS)

  if (${CONFIGURATION_TYPE_CAPS} IN_LIST RBFX_BUILD_TYPES)
    set(RBFX_DLL_PATH "")

    if (DEFINED RBFX_SDK_PATH)
      get_property(RBFX_DLL_PATH
        TARGET Urho3D
        PROPERTY IMPORTED_LOCATION_${CONFIGURATION_TYPE_CAPS})
    else ()
      get_property(URHO_DLL_BIN_DIR
        TARGET Urho3D
        PROPERTY RUNTIME_OUTPUT_DIRECTORY)
      # TODO we had Debug/ here
      set(RBFX_DLL_PATH "${URHO_DLL_BIN_DIR}/${PROCESS_DLL_CONFIGURATION_TYPE}/Urho3D.dll")
    endif()

    message("will use ${PROCESS_DLL_CONFIGURATION_TYPE} ${URHO_LIB_PREFIX}Urho3D.dll from ${RBFX_DLL_PATH}")
    install(FILES "${RBFX_DLL_PATH}"
      CONFIGURATIONS ${PROCESS_DLL_CONFIGURATION_TYPE}
      DESTINATION ".")

    add_dll_symlink_target(
      CONFIGURATION_TYPE ${PROCESS_DLL_CONFIGURATION_TYPE}
      RBFX_DLL_PATH ${RBFX_DLL_PATH})
  else ()
    message(WARNING "Urho3D ${PROCESS_DLL_CONFIGURATION_TYPE} dll not found!")
  endif()

  if (EXISTS "${CMAKE_BINARY_DIR}/${PROCESS_DLL_CONFIGURATION_TYPE}" AND ${CONFIGURATION_TYPE_CAPS} IN_LIST RBFX_BUILD_TYPES)
    execute_process (COMMAND ${CMAKE_COMMAND} -E create_symlink  ${RBFX_DLL_PATH} ${CMAKE_BINARY_DIR}/${PROCESS_DLL_CONFIGURATION_TYPE}/${URHO_LIB_PREFIX}Urho3D.dll)
  endif ()

endfunction()


if(MINGW)
  # TODO better write from scratch
endif()

if(MSVC)
  bring_urho_dll_in(CONFIGURATION_TYPE Release)
  bring_urho_dll_in(CONFIGURATION_TYPE RelWithDebInfo)
  bring_urho_dll_in(CONFIGURATION_TYPE Debug)
endif()

