#pragma once

#include <vector>
#include <algorithm>

#include <VcppBits/MathUtils/MathUtils.hpp>

#include "TreeGenerator.hpp"

namespace UrhoBits {

class NurbsIter {
public:
    NurbsIter (const size_t pNumSourceSegments,
               const size_t pNumResultSegments,
               const std::vector<Branch::ChildrenData> &pChildren,
               bool pFixLen)
        : _numResultSegments (std::max(size_t(1), pNumResultSegments)),
          _incr (float(pNumSourceSegments) / pNumResultSegments),
          _end (std::max(size_t(1), pNumSourceSegments)),
          _children (pChildren),
          _fixLen (pFixLen) {
        for (const auto &el : _children) {
            if (el.position == 0.f) { // TODO proper float comp
                _childIndex = el.index;
                _previousChildIndex = el.index;
            }
        }

        processFixLenCalcs();
    }

    bool isDone () {
        return _i > _end;
    }

    size_t getNextChildSearchPos () {
        size_t i = 0;
        if (_previousChildIndex < _children.size()
            && _previousChildIndex + 1 < _children.size()) {
            i = _previousChildIndex + 1;
        }

        return i;
    }

    bool processChildCalcs() {
        for (size_t i = getNextChildSearchPos(); i < _children.size(); ++i) {
            const auto& el = _children[i];
            const auto ii = el.position * _end;
            if (ii > _i
                // multiplying by a small value here, otherwise it might be
                // just a tiny fraction too short on approach to child
                && ii <= (_i + _incr * _fixLenMulti * 1.01f)) {
                _i = ii;
                _childIndex = el.index;
                _previousChildIndex = i;
                _fixLenMulti = 1.f;
                _previousMandatorySplitPos = el.position;
                return true;
            }
        }
        return false;
    }

    void advance () {
        if (processChildCalcs()) {
            if (_i < _end) {
                processFixLenCalcs();
            }
            return;
        }

        if (_i + _incr * _fixLenMulti < _end - _incr * _fixLenMulti / 10.f) {
            _i += _incr * _fixLenMulti;
        } else if (_i < _end) {
            _i = _end;
        } else {
            _i = _end + _incr * _fixLenMulti; // will properly indicate end
        }

        _childIndex = std::numeric_limits<size_t>::max();

    }

    void processFixLenCalcs () {
        if (!_fixLen) {
            return;
        }

        const auto ci = getNextChildIndex();
        const float pos = (ci < _children.size()
                           ? _children[ci].position
                           : 1.f);

        const float would_have_had_actual_num_segments =
            pos * _numResultSegments
            - _previousMandatorySplitPos * _numResultSegments;

        const float remainder =
            VcppBits::MathUtils::getDecimalPart(would_have_had_actual_num_segments);

        const int will_make_it_this_many = int(would_have_had_actual_num_segments)
            + (remainder > 0.2f ? 1 : 0);

        _fixLenMulti =
            would_have_had_actual_num_segments / (will_make_it_this_many);
    }


    float getCurrent () const {
        return _i;
    }

    size_t getChildIndex () const {
        return _childIndex;
    }

    size_t getNextChildIndex () {
        size_t i = _previousChildIndex +1;
        if (i < _children.size()) {
            return i;
        }
        return std::numeric_limits<size_t>::max();
    }

private:
    float _i = 0.f;
    float _fixLenMulti = 1.f;
    size_t _numResultSegments;

    float _incr;
    float _end;

    float _previousMandatorySplitPos = 0.f;
    size_t _previousChildIndex = std::numeric_limits<size_t>::max();
    size_t _childIndex = std::numeric_limits<size_t>::max();

    const std::vector<Branch::ChildrenData> &_children;

    bool _fixLen = false;
};


} // namespace UrhoBits
