// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#include "TreeGenerator.hpp"




// TODO cleanup redundant includes
#include <algorithm>
#include <cmath>
#include <tuple>

#include <Urho3D/IO/Log.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Math/Quaternion.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Technique.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/RenderPath.h>
#include <Urho3D/UI/UI.h>

#include <VcppBits/MathUtils/MathUtils.hpp>
#include <VcppBits/SimpleVector/SimpleVector.hpp>

#include "Utils.hpp"
#include "TreeGeneratorGeometry.hpp"
#include "TrunkVertexElement.hpp"

namespace UrhoBits {

using namespace Urho3D;

using VcppBits::MathUtils::clamp;
using VcppBits::MathUtils::interp_bezier;
using VcppBits::MathUtils::lerp;
using VcppBits::MathUtils::sign;

namespace detail {

Vector3 get_outward_direction (const Vector3 &vec,
                               const float pOutwardAngle) {
    Quaternion orientation;
    orientation.FromRotationTo(Vector3::UP, vec);


    Vector3 v(1 * Cos(pOutwardAngle), 0, Sin(pOutwardAngle));
    return orientation * v;
}

int calcNumChildren (const float pBranchLength,
                     const float pChildrenPerMeter,
                     const int pChildrenMin,
                     const float pOffset) {
    const float relevant_length = pBranchLength * (1.f - pOffset);
    return std::max(int(pChildrenPerMeter * relevant_length), pChildrenMin);
}


// position, direction, radius
std::tuple<Vector3, Vector3, float>
find_child_pos (const Branch * pParent, const float pPosition) {
    auto pos = pParent->rootPos;
    auto dir = pParent->segments[0].vec.Normalized();
    const auto dest_len = pParent->length * pPosition;
    auto tot_len = 0.f;
    auto radius = pParent->rootRadius;

    for (const auto& seg : pParent->segments) {
        const auto seg_len = seg.vec.Length();
        if (tot_len + seg_len > dest_len) {
            const auto len_left = dest_len - tot_len;
            const auto multiplier = len_left / seg_len;
            pos += seg.vec * multiplier;
            radius = lerp(radius, seg.endRadius, multiplier);
            dir = lerp(dir, seg.vec / seg_len, multiplier);
            break;
        }
        tot_len += seg_len;
        pos += seg.vec;
        dir = seg.vec / seg_len;
        radius = seg.endRadius;
    }

    return std::make_tuple(pos, dir, radius);
}


float calc_crown_center (float pOffset, float pPosition = 0.5f) {
    return (1.f - pOffset) * pPosition + pOffset;
}


float adjust_offset (float pOffset,
                     float pBranchLengthMultiplier,
                     float pCancelAmount) {
    // find how much shorter already we are
    auto already_applied_offset = 1.f - pBranchLengthMultiplier;

    const float adjusted = clamp(pOffset - already_applied_offset, 0.f, 1.f);

    // now mix it with original offset
    return lerp(pOffset, adjusted, pCancelAmount);
}


float calcBranchLengthMultiplier (std::mt19937 &pMt,
                                  const TreeConfig::LevelSettings &ch,
                                  const float pChildPosition) {
    float equal_k = ch.length_distribution_equal;
    float circle_k = ch.length_distribution_circle;
    float conical_k = std::fabs(ch.length_distribution_cone);
    float flame_k = ch.length_distribution_flame;
    float random_k = ch.length_distribution_random;

    float sum = circle_k + conical_k + flame_k + random_k + equal_k;
    if (sum < 0.001f) {
        equal_k = 1.f;
        sum = 1.f;
    }
    equal_k /= sum;
    circle_k /= sum;
    conical_k /= sum;
    flame_k /= sum;
    random_k /= sum;

    const float circle_function
        = (-((pChildPosition*2.f - 1.f)*(pChildPosition*2.f - 1.f)) + 1.f);

    const float circle_part = circle_function * circle_k;

    const float conical_function =
        (ch.length_distribution_cone > 0)
        ? (1 - pChildPosition)
        : (pChildPosition);
    const float conic_part = conical_function * conical_k;

    const float flame_function =
        (pChildPosition <= 0.3f)
        ? (pChildPosition / 0.3f)
        : ((1.f - pChildPosition) / 0.7f);
    const float flame_part = flame_function * flame_k;

    std::uniform_real_distribution<float> dist (0.f, 2.f);

    const float rand_part = dist(pMt) * random_k;

    const float equal_part = equal_k;


    return circle_part + conic_part + flame_part + rand_part + equal_part;
}


Vector3 calc_branch_initial_direction (std::mt19937 &pMt,
                                       const TreeConfig::LevelSettings &pCh,
                                       const Vector3 &pParentDirection,
                                       const Vector3 &pOutwardDirection,
                                       const Vector3 &pCrownOutwardDirection) {
    std::uniform_real_distribution<float> dist (-1.0f , 1.0f);

    const Vector3 deviation = Vector3(dist(pMt), dist(pMt), dist(pMt));

    return (pOutwardDirection * pCh.outward_k
            + Vector3::UP * pCh.zward_k
            + deviation * pCh.deviation_k
            + pParentDirection * pCh.parent_k
            + pCrownOutwardDirection * pCh.crown_outward_k).Normalized();
}

Vector3 calc_endpoint_vector (std::mt19937 &pMt,
                              const TreeConfig::LevelSettings &pCfg,
                              const Vector3 pParentDirection,
                              const Vector3 &pOutwardDirection,
                              const Vector3 &pCrownOutwardDirection) {
    std::uniform_real_distribution<float> dist(-pCfg.endpoint_random_k,
                                               pCfg.endpoint_random_k);
    const Vector3 endpoint_random = Vector3(dist(pMt), dist(pMt), dist(pMt));
    return (pOutwardDirection * pCfg.endpoint_outward_k
              + Vector3::UP * pCfg.endpoint_Zward_k
              + pParentDirection * pCfg.endpoint_parent_k
              + pCrownOutwardDirection * pCfg.endpoint_crown_outward_k
              + endpoint_random).Normalized();
}

Branch create_smooth_branch (const Branch &pBr,
                                            const int pSmoothingIterations,
                                            const float pThreshold) {

    if (pBr.segments.size() == 1) {
        return pBr;
    }

    Branch br { pBr.rootPos,
                               pBr.rootRadius,
                               pBr.length,
                               pBr.lengthMultiplier,
                               pBr.parentRadius,
                               {},
                               pBr.children };

    // TODO prealloc (.reserve()) br.segments?

    const float curve_resolution = pSmoothingIterations;

    Vector3 begin_pos = pBr.rootPos;
    for (size_t i = 0; i < pBr.segments.size() - 1; ++i) {
        const Vector3 vec = pBr.segments[i].vec;
        const Vector3 vec2 = pBr.segments[i + 1].vec;
        const Vector3 p_0 = begin_pos + ((i == 0) ? Vector3{} : vec / 2.f);
        const Vector3 p_1 = begin_pos + vec;
        const Vector3 p_2 = p_1 + ((i == pBr.segments.size() - 2) ? vec2  : (vec2 / 2.f));

        Vector3 prev_B = p_0;
        Vector3 prev_normal = vec.Normalized();

        for (int j = 0; j < pSmoothingIterations; ++j) {
            const float incr = 1.f / curve_resolution;

            const float t =  incr * float(j+1);

            const Vector3 B = interp_bezier(p_0, p_1, p_2, t);
            const Vector3 normal = B - prev_B;
            auto angle = prev_normal.Angle(normal);
            if (angle > pThreshold || j == pSmoothingIterations - 1) {
                const float radius2 = lerp(pBr.getNthBranchRadius(i),
                                           pBr.getNthBranchRadius(i+1),
                                           t);

                br.segments.push_back({ B - prev_B, radius2});
                prev_B = B;
                prev_normal = normal;
            }
        }

        begin_pos += vec;
    }

    if (br.segments.size() == 0) {
        throw;
    }

    return br;
}


struct Vector3NurbsAdp {
    const Vector3 &get (const Branch::Segment& pSegment) const {
        return pSegment.vec;
    }
};


Branch create_precise_smooth_branch (
        const Branch &pBr,
        const float pSubdivisionsPerMeter,
        const float pThreshold,
        Branches &pTreeData,
        float pBranchingDirectionMirror,
        bool pFixLen) {

    if (pBr.segments.size() == 1) {
        return pBr;
    }

    Branch br { pBr.rootPos,
                pBr.rootRadius,
                pBr.length,
                pBr.lengthMultiplier,
                pBr.parentRadius,
                {},
                pBr.children,
                pBr.parent };

    // TODO prealloc (.reserve()) br.segments?

    const auto curve =
        VcppBits::MathUtils::NurbsCurveWalker<Vector3,
                                              Branch::Segment,
                                              Vector3NurbsAdp> (pBr.segments);

    Vector3 prev_B = pBr.rootPos; // this one doesn't contain applied branch
                                  // children mirror rotation
    Vector3 real_prev_B = pBr.rootPos; // this contains branch children mirror
                                       // rotation
    Quaternion rot;

    NurbsIter iter (pBr.segments.size(),
                    pBr.length * pSubdivisionsPerMeter,
                    pBr.children,
                    pFixLen);
    if (iter.getChildIndex() != size_t(-1)) {
        // TODO: the hell is this?
        pTreeData[iter.getChildIndex()].rootPos = real_prev_B;
    }
    iter.advance();
    for (; !iter.isDone(); iter.advance()) {
        const float i = iter.getCurrent();

        const Vector3 B = curve.getPos(i) + pBr.rootPos;
        const float radius2 = lerp(pBr.getNthBranchRadius(size_t(i)),
                                   pBr.getNthBranchRadius(size_t(i+1)),
                                   i - float(int(i)));

        Vector3 new_segment  = (B - prev_B);

        new_segment = rot * pBranchingDirectionMirror * new_segment;

        real_prev_B += new_segment;
        if (iter.getChildIndex() != size_t(-1)) {
            // branch mirror rotation
            auto &child = pTreeData[iter.getChildIndex()];
            child.parentRadius = radius2;
            Vector3 child_dir = child.segments[0].vec;

            // q is our point -- child.segments[0].vec
            // p is 0
            // n is segment
            const auto n = new_segment.Normalized();
            const auto p = child.segments[0].vec.Normalized();

            Vector3 q_proj = (p - p.DotProduct(n) * n).Normalized();

            child.rootPos = real_prev_B + q_proj * child.parentRadius;

            rot.FromRotationTo(child_dir, new_segment);
        } else if (br.segments.size()
                   && new_segment.Angle(br.segments.back().vec) < pThreshold) {
            br.segments.back().vec += new_segment;
            br.segments.back().endRadius = radius2;
            prev_B = B;
            continue;
        }


        br.segments.push_back({ new_segment, radius2});
        prev_B = B;
    }
    if (br.segments.size() == 0) {
        throw;
    }

    return br;
}


} // namespace detail



TreeGenerator::TreeGenerator ()
    : _mt (0),
      _mt_lf (0){
}


// puts the branch to tree_data. TODO: make it return it and put it to data in a callee
Branch create_branch (std::mt19937 &_mt,
                      const TreeConfig& _cfg,
                      const Branch * pParent,
                      const float pPosition,
                      const float pLength,
                      const float pOutwardAngle,
                      const size_t pParentIndex,
                      const size_t pParentLevel) {
    const auto &ch = _cfg.levels[pParentLevel + 1];

    Branch br { Vector3(0, 0, 0),
                _cfg.root_radius,
                pLength,
                1.f,
                1.f,
                {},
                {},
                pParentIndex
    };
    Vector3 normal = Vector3::UP;

    Vector3 parent_direction;

    if (pParent) {
        std::tie(br.rootPos, parent_direction, br.parentRadius)
            = detail::find_child_pos(pParent, pPosition);
    }

    const Vector3 outward =
        detail::get_outward_direction(parent_direction,
                                      pOutwardAngle);

    const float k = pPosition - ch.crownpos_offset - 0.5f;
    const Vector3 crown_outward =
        (k * parent_direction
         + outward * (1.f - std::abs(k))).Normalized();

    if (pParent) {
        normal = detail::calc_branch_initial_direction(
            _mt, ch, parent_direction, outward, crown_outward);
        br.rootRadius = br.parentRadius * ch.branching_radius_multiplier;
    } else {
        parent_direction = normal;
    }

    float radius = br.rootRadius;

    const auto endpoint_vec = detail::calc_endpoint_vector(
        _mt, ch, parent_direction, outward, crown_outward);
    const float actual_segment_length = pLength / (float)ch.num_segments;
    const float end_radius = br.rootRadius * ch.radius_multi;

    Vector3 total_branch_vec; // TODO cleanuP?
    for (int i = 0; i < ch.num_segments; ++i) {
        const float endpoint_k = (float)i / (float) ch.num_segments * ch.endpoint_influence;
        const float inv_endpoint_k = std::max(1.f - endpoint_k, 0.f);
        std::uniform_real_distribution<float> gr_dist (-ch.growth_dev, ch.growth_dev);

        const Vector3 new_normal =
            (normal + Vector3(gr_dist(_mt),
                              gr_dist(_mt),
                              gr_dist(_mt))).Normalized();

        const float radius_k = ((float)(i + 1)) / (float)ch.num_segments;
        radius = radius_k * end_radius + (1.f - radius_k) * br.rootRadius;

        const Vector3 res = (new_normal * inv_endpoint_k
                             + endpoint_vec * endpoint_k).Normalized();

        br.segments.push_back({ res * actual_segment_length, radius});
        total_branch_vec += res * actual_segment_length;
    }

    return br;
}

Leaf* create_leaf (std::deque<Leaf> &pLeafData,
                   const TreeConfig &pCfg,
                   std::mt19937 &pRng,
                   const Branch * pParent,
                   const float pPosition,
                   const float pOutwardAngle,
                   const size_t pParentLevel,
                   const Vector3 &pCrownCenter,
		   const size_t pLeftOrRight) {
    const size_t i_lf = size_t(pCfg.num_levels) - pParentLevel;
    const auto &ch = pCfg.leaves[i_lf];
    pLeafData.push_back(Leaf{});

    Leaf &lf = pLeafData.back();
    //Vector3 normal = Vector3::UP;

    Vector3 parent_direction;

    std::tie(lf.pos, parent_direction, std::ignore)
        = detail::find_child_pos(pParent, pPosition);

    auto outw = detail::get_outward_direction(parent_direction, pOutwardAngle);
    auto left_and_right_dir =
      (parent_direction.CrossProduct(Vector3::UP) * (((int)(pLeftOrRight % 2)) * 2 - 1)).Normalized();

    // by doing this integer random generation we try to reduce amount of entropy 
    // required -- the leaves are going to be put along the branch anyway, 
    // so it's unlikely that we gonna spot any regularity
    std::uniform_int_distribution<int16_t> dist(-15, 15);
    Vector3 endpoint_random =
        Vector3(float(dist(pRng))/15.f,
                float(dist(pRng))/15.f,
                float(dist(pRng))/15.f).Normalized();

    const Vector3 crown_outward = (lf.pos - pCrownCenter).Normalized();

    lf.vec =
        (left_and_right_dir * ch.lf_direction_left_and_right_k
         + outw * ch.lf_endpoint_outward_k
         + Vector3::UP * ch.lf_endpoint_Zward_k
         + parent_direction * ch.lf_endpoint_parent_k
         + endpoint_random * ch.lf_endpoint_random_k
         + crown_outward * ch.lf_endpoint_crown_outward_k)
        .Normalized() * pCfg.lf_leaf_size;

    auto _normalized_conf = Vector3(ch.lf_normal_parent_k,
                                    ch.lf_normal_outward_k,
                                    ch.lf_normal_upward_k).Normalized();

    auto normal_lookup_parent = _normalized_conf.x_;
    auto normal_lookup_outward = _normalized_conf.y_;
    auto normal_lookup_upward = _normalized_conf.z_;

    auto normal_lookup = normal_lookup_parent * parent_direction
        + normal_lookup_outward * outw
        + normal_lookup_upward * Vector3(0.f, 1.f, 0.f);

    lf.norm =
        -( normal_lookup.CrossProduct(lf.vec).CrossProduct(lf.vec).Normalized());


    return &lf;
}


std::vector<float> spread_angles(const size_t pNumAngles) {
    std::vector<float> outward_angles;

    outward_angles.reserve(pNumAngles);
    for (size_t i = 0; i < pNumAngles; ++i) {
        outward_angles.push_back(float((i * 140) % 360));
    }

    return outward_angles;
}


void fill_leaves (std::deque<Leaf> &pLeafData,
                  const TreeConfig &pCfg,
                  std::mt19937 &pRng,
                  const Branch * pBranch,
                  const size_t pParentLevel,
                  const Vector3 &pCrownCenter) {
    const int num_children = detail::calcNumChildren(pBranch->length,
						     (float)pCfg.leaves_per_meter,
						     pCfg.leaves_per_meter > 0, 
						     pCfg.leaves_offset);

    if (!num_children) {
        return;
    }

    std::vector<float> outward_angles = spread_angles(num_children);

    float incr;
    float offset = pCfg.leaves_offset;
    if (num_children > 1) {
      incr = clamp((1.f - offset) / ((float)(num_children - 1)),
		   0.0001f,
		   1.f);
    } else {
      incr = 0.f;
      offset = 1.f; // move the only leaf to branch end
    }


    std::uniform_real_distribution<float>
        dist (-incr/4, incr/4); //TODO make configurable impact on the increment

    for (size_t i = 0; i < num_children; ++i) {
        float outward_angle = outward_angles[i];

        const float pos = // TODO improve these calculations to get rid of clamp?
            clamp(offset + i * incr + dist(pRng), 0.f, 1.f);

        create_leaf(pLeafData, pCfg, pRng, pBranch, pos, outward_angle, pParentLevel, pCrownCenter, i);
    }
}

// supposed to be called with a parent branch
void fill_children (std::mt19937 &_mt,
                    BranchesLevel &tree_data,
                    const TreeConfig& _cfg,
                    const TreeConfig::LevelSettings &ch,
                    Branch *pBranch,
                    const size_t pParentLevel,
                    const size_t pParentIndex,
                    const int pNumChildren,
                    const float pOffset,
                    const float pAcacia) {
    float num_children_float = float(pNumChildren);
    float incr;
    float offset = pOffset;
    if (num_children_float > 1) {
        incr = clamp((1.f - pOffset) / (num_children_float - 1.f),
                      0.0001f,
                      1.f);
    } else {
        incr = 0.f;
        offset += (1.f - pOffset) / 2.f; // move the only branch to center
    }

    const float deviation = 0.5f; // TODO configurable

    std::uniform_real_distribution<float>
        dist (-incr * deviation, incr * deviation);

    std::vector<float> outward_angles = spread_angles(size_t(pNumChildren));

    float acacia_pos_sum = 0.f;
    std::vector<std::tuple<Branch*, float>> children; // TODO use ChildrenData
    children.reserve(size_t(pNumChildren));

    for (size_t i = 0; i < size_t(pNumChildren); ++i) {
        float outward_angle = outward_angles[i];
        const float pos =
            clamp(offset + i * (incr) + dist(_mt), 0.f, 1.f);

        const float branch_length_multiplier =
            detail::calcBranchLengthMultiplier (_mt, ch, pos);

        const float branch_length =
            pBranch->length * ch.length_multiplier * branch_length_multiplier;


        if (branch_length < 0) {
            continue;
        }
        auto &br = tree_data.push_back(create_branch(_mt,
                                                     _cfg,
                                                     pBranch,
                                                     pos,
                                                     branch_length,
                                                     outward_angle,
                                                     pParentIndex,
                                                     pParentLevel));

        // this value is saved to array for quick lookup later
        pBranch->children.push_back({ pos, tree_data.getCurrentIndex() - 1 });
        br.lengthMultiplier = branch_length_multiplier;
        children.push_back({&br, 0.f});

        // prepare data for acacia
        if (pAcacia > 0.f) {
            std::get<1>(children.back()) = br.rootPos.y_;
            for (Branch::Segment &s : br.segments) {
                std::get<1>(children.back()) += s.vec.y_;
            }
            acacia_pos_sum += std::get<1>(children.back());
        }
    }

    // reusing the variable of same type heh :/
    acacia_pos_sum /= num_children_float;

    for (auto &el: children) {
        Branch* br = std::get<0>(el);
        const float& br_z_end_pos = std::get<1>(el);

        if (pAcacia > 0.f) {
            const float &avg_pos = acacia_pos_sum;
            const float diff = br_z_end_pos - avg_pos;
            const float acacia_incr = diff / float(br->segments.size());
            // TODO: should we recalculate the length (and longestChild)?
            for (Branch::Segment &s : br->segments) {
                s.vec.y_ -= acacia_incr * pAcacia;
            }
        }
    }
}

void TreeGenerator::init (Scene* pScene,
                          const bool pDebugRender = false) {
    _context = pScene->GetContext();
    _scene = pScene;
    _cache = pScene->GetSubsystem<ResourceCache>();
    _debugRender = pDebugRender;
    _isInitialized = true;

    regenerate();
}

/* TODO10: unused?
// position, direction, radius
std::tuple<Vector3, Vector3, float>
TreeGenerator::getParentData (const size_t pChildIndex,
                              const float pPosOffset = 0.f) const {
    if (pChildIndex == 0) {
        throw;
    }
    auto it = std::upper_bound(_parentIndexes.begin(),
                               _parentIndexes.end(),
                               std::pair<size_t, size_t>(pChildIndex, 0),
                               [&pChildIndex](const auto &el1, const auto& el2) {
                                   return el1.first < el2.first;
                           });
    --it;

    const auto &ch_data = tree_data[it->second].children;
    auto ch_it = std::find_if(ch_data.begin(),
                              ch_data.end(),
                              [&pChildIndex](const auto &el) {
                                  return el.index == pChildIndex;
                              });

    return detail::find_child_pos(&tree_data[it->second],
                                  clamp(ch_it->position + pPosOffset, 0.f, 1.f));
}

*/

void TreeGenerator::_clearData () {
    _bbTrunc = BoundingBox();
    _bbTrunc.Merge(Vector3::ZERO);
    _bbLeaves = BoundingBox();
    _bbLeaves.Merge(Vector3::ZERO);

    dt.resize(1);
    dt.back().index_data.resize(0);
    dt.back().vertex_data.resize(0);
    dt_leaf.resize(1);
    dt_leaf.back().index_data.resize(0);
    dt_leaf.back().vertex_data.resize(0);
    dt_portion.index_data.resize(0);
    dt_portion.vertex_data.resize(0);

    tree_data.clear();
    leaf_data.clear();
    _mt.seed(unsigned(_cfg->seed));
    _mt_lf.seed(unsigned(_cfg->seed));
    _branchDataIndexIndexes.clear();
}


void generate_tree_data (
        Branches &tree_data,
        Urho3D::Vector3 &_treeCrownCenter,
        const TreeConfig &_cfg,
        std::mt19937 &_mt) {
    auto &root = tree_data.addLevel().push_back(create_branch(_mt, _cfg, nullptr, 0, _cfg.root_length, 0, size_t(-1), size_t(-1)));

    // TODO: consider configurable or just some other position than 0.5
    _treeCrownCenter =
        std::get<0>(
            detail::find_child_pos(&root,
                                   detail::calc_crown_center(_cfg.levels[0].offset,
                                                             0.5f)));
    size_t prev_level_start = 0;
    size_t prev_level_end = 1; // assuming we always have 1 root
    for (size_t i = 0; i < (size_t)_cfg.num_levels; ++i) {
        const auto &lvl = _cfg.levels[i];
        auto branches_level  = tree_data.addLevel();
        for (size_t j = prev_level_start; j < prev_level_end; ++j) {
            const float adjusted_offset = (i > 0)
                ? lvl.offset
                : detail::adjust_offset(lvl.offset,
                                        tree_data[j].lengthMultiplier,
                                        lvl.offset_shortbranch_cancel);

            fill_children(_mt,
                          branches_level,
                          _cfg,
                          _cfg.levels[i + 1],
                          &tree_data[j],
                          i,
                          j,
                          detail::calcNumChildren(tree_data[j].length,
                                                  lvl.children_per_meter,
                                                  lvl.children_min,
                                                  adjusted_offset),
                          adjusted_offset,
                          lvl.acacia);
        }

        prev_level_start = prev_level_end;
        prev_level_end = tree_data.size();
    }
}

// TODO: name is wrong, as it also does branch smoothing
void generate_leaves_data (std::deque<Leaf> &pLeafData,
                           Branches &pBranches,
                           const TreeConfig &pCfg,
                           std::mt19937 &pRng,
                           const Vector3 &pCrownCenter) {
    for (size_t level_i = 0; level_i < pBranches.getNumLevels(); ++level_i) {
        for (auto &br : pBranches.getLevel(level_i)) {
            br = detail::create_precise_smooth_branch(
                br,
                pCfg.levels[level_i].subdivisions_per_meter,
                pCfg.branch_curvature_threshold,
                pBranches,
                pCfg.levels[level_i].branching_mirror_direction,
                pCfg.levels[level_i].fix_lengths);

            if ((pCfg.num_levels - level_i) < size_t(pCfg.leaves_level)) {
                fill_leaves(pLeafData, pCfg, pRng, &br, level_i, pCrownCenter);
            }
        }
    }
}

void append_model(LeavesDataFormat &pDt,
                  Urho3D::Model *pCustomLeavesModel,
                  BoundingBox &pBounds,
                  const Urho3D::Vector3 &pPos,
                  const Urho3D::Vector3 &pDir,
                  const Urho3D::Vector3 &pOutwardNormal,
                  const float pNormalsOutwardK) {
    auto vbuffer = pCustomLeavesModel->GetVertexBuffers()[0];
    auto data = vbuffer->GetShadowData();
    const auto pos_offset =
        vbuffer->GetElement(VertexElementSemantic::SEM_POSITION)->offset_;
    const auto uv_offset =
        vbuffer->GetElement(VertexElementSemantic::SEM_TEXCOORD)->offset_;
    const auto normal_offset =
        vbuffer->GetElement(VertexElementSemantic::SEM_NORMAL)->offset_;
    const auto tangent_offset =
        vbuffer->GetElement(VertexElementSemantic::SEM_TANGENT)->offset_;

    const unsigned short vertex_index_offset = pDt.vertex_data.size();

    for (unsigned i = 0; i < vbuffer->GetVertexCount(); ++i) {
        auto el = i * vbuffer->GetVertexSize();
        Quaternion transform_q{Vector3::FORWARD, pDir};
        const Vector3 pos =
            transform_q
            * *((Vector3*) (data + el + pos_offset)) + pPos;
        const Vector2 uv = *((Vector2*) (data + el + uv_offset));
        const Vector3 norm = lerp(Quaternion {Vector3::FORWARD, pDir}
                                  * *((Vector3*) (data + el + normal_offset)),
                                  pOutwardNormal,
                                  pNormalsOutwardK).Normalized();
        Vector4 tangent = *((Vector4*)(data + el + tangent_offset));
        Vector3 *tangent3 = (Vector3*) &tangent;
        *tangent3 = transform_q * (*tangent3);
        pDt.vertex_data.push_back({pos, norm, tangent, uv});
        pBounds.Merge(pos);
    }

    auto ibuffer = pCustomLeavesModel->GetIndexBuffers()[0];
    auto idata = (unsigned short*) ibuffer->GetShadowData(); // TODO indx sizes
    const auto offset = vertex_index_offset;
    for (unsigned short i = 0; i < (ibuffer->GetIndexCount() / 3); ++i) {
        auto* el = idata + i * 3;
        pDt.index_data.push_back({ static_cast<unsigned short>(*el + offset),
	                           static_cast<unsigned short>(*(el + 1) + offset),
				   static_cast<unsigned short>(*(el + 2) + offset) });
    }
}

void generate_leaves_geometry (std::deque<LeavesDataFormat> &pDtDest,
                               LeavesDataFormat &pDtPortion,
                               std::mt19937 &pRng,
                               BoundingBox &pBounds,
                               const std::deque<Leaf> &pLeafData,
                               const TreeConfig &pCfg,
                               const Vector3 &pTreeCrownCenter,
                               Urho3D::Model *pCustomLeavesModel) {
    const float normals_outw_k = pCfg.leaves_normals_outwards;
    for (auto lf : pLeafData) {
        const Vector3 outward_normal = (lf.pos - pTreeCrownCenter).Normalized();
        PortionedDataAppender po (pDtDest, pDtPortion);
        auto norm = normals_outw_k * outward_normal
            + (1.f - normals_outw_k) * lf.norm;

        std::uniform_int_distribution<int> dist_x (0, pCfg.leaves_atlas_res_x - 1);
        std::uniform_int_distribution<int> dist_y (0, pCfg.leaves_atlas_res_y - 1);
        const float leaf_coord_incr_x = 1.f / float(pCfg.leaves_atlas_res_x);
        const float leaf_coord_incr_y = 1.f / float(pCfg.leaves_atlas_res_y);


        switch (pCfg.leaves_shape) {
        case TreeConfig::LeafShape::DIAMOND:
            create_diamond_leaf(
                pDtPortion.index_data,
                pDtPortion.vertex_data,
                lf.pos,
                lf.vec,
                lf.norm,
                norm,
                Vector2(pCfg.leaves_atlas_res_x,
                        pCfg.leaves_atlas_res_y),
                Vector2(leaf_coord_incr_x * float(dist_x(pRng)),
                        leaf_coord_incr_y * float(dist_y(pRng))),
                pBounds);
            break;
        case TreeConfig::LeafShape::SQUARE:
            create_square_leaf(
                pDtPortion.index_data,
                pDtPortion.vertex_data,
                lf.pos,
                lf.vec,
                lf.norm,
                norm,
                Vector2(pCfg.leaves_atlas_res_x,
                        pCfg.leaves_atlas_res_y),
                Vector2(leaf_coord_incr_x * float(dist_x(pRng)),
                        leaf_coord_incr_y * float(dist_y(pRng))),
                pBounds);
            break;
        case TreeConfig::LeafShape::POINT_STUB:
        {
            pDtPortion.vertex_data.push_back({lf.pos, lf.vec, Vector4{}, Vector2{}});

            // even if point is at the branch, that branch might not be
            // rendered, so we better off always recalc bounds:
            pBounds.Merge(lf.pos);

            // TODO4: refactor into function, same as in
            // TreeGeneratorGeometry.cpp::create_*leaf
            const unsigned short i =
                (unsigned short) std::min(
                    pDtPortion.vertex_data.size() - 1,
                    size_t(std::numeric_limits<unsigned short>::max() - 3));

            pDtPortion.index_data.push_back({i, i, i});
        }
        break;
        case TreeConfig::LeafShape::OBJECT:
        {
            if (pCustomLeavesModel) {
                append_model(pDtPortion,
                             pCustomLeavesModel,
                             pBounds,
                             lf.pos,
                             lf.vec,
                             outward_normal,
                             normals_outw_k);
            }
        }
        break;
        }
    }
}

void generate_tree_geometry (
        const Branches &pBranches,
        const TreeConfig &_cfg,
        std::vector<std::pair<size_t, size_t>> &_branchDataIndexIndexes,
        std::deque<TrunkDataFormat> &dt,
        TrunkDataFormat &dt_portion,
        BoundingBox &_bbTrunc) {
    auto levels_to_render = (pBranches.getNumLevels() -
                             clamp(size_t(_cfg.norender_levels),
                                   size_t(0),
                                   pBranches.getNumLevels()));
    for (size_t level_i = 0; level_i < levels_to_render; ++level_i) {
        for (auto &branch : pBranches.getLevel(level_i)) {
            const auto &ch = _cfg.levels[level_i];
            // assuming there's atleast one segment
            PortionedDataAppender po (dt, dt_portion);
            if (branch.segments.size() == 0) {
                continue; // TODO consider triggering some action or error report?
            }
            auto seg1 = branch.segments[0];
            Vector3 end_normal =
                // if there is a continuation, then use average of current and next
                // normals:
                (branch.segments.size() > 2)
                ? (branch.segments[1].vec.Normalized()
                   + branch.segments[0].vec.Normalized()).Normalized()
                : branch.segments[0].vec.Normalized();

            if (level_i > 0) {
                const auto parent_indexes =
                    _branchDataIndexIndexes[branch.parent];
                const auto parent_arr = parent_indexes.first;
                const auto parent_offs = parent_indexes.second;
                size_t parent_index_count;
                const auto &parent_dt = dt[parent_arr];

                if (branch.parent + 1 < _branchDataIndexIndexes.size()
                    && _branchDataIndexIndexes[branch.parent + 1].first == parent_arr) {
                    parent_index_count = _branchDataIndexIndexes[branch.parent + 1].second
                        - parent_offs;
                } else {
                    parent_index_count =
                        parent_dt.index_data.size() - parent_offs;
                }

                create_weilded_segment(parent_dt.index_data,
                                       parent_dt.vertex_data,
                                       parent_offs * 3,
                                       parent_index_count,
                                       dt_portion.index_data,
                                       dt_portion.vertex_data,
                                       branch.rootPos,
                                       branch.rootRadius,
                                       branch.rootPos + seg1.vec,
                                       seg1.endRadius,
                                       ch.circle_resolution,
                                       end_normal,
                                       0.f,
                                       _bbTrunc,
                                       ch.weild);
            } else {
                create_segment(dt_portion.index_data,
                               dt_portion.vertex_data,
                               branch.rootPos,
                               branch.rootRadius,
                               branch.rootPos + seg1.vec,
                               seg1.endRadius,
                               ch.circle_resolution,
                               end_normal,
                               0.f,
                               _bbTrunc);
            }
            auto prev_root_pos = branch.rootPos + seg1.vec;
            float l = seg1.vec.Length();


            for (size_t ii = 1; ii <  branch.segments.size(); ++ii) {
                bool isnt_last = ii + 1 < branch.segments.size();
                const auto &segment = branch.segments[ii];

                const Vector3 continue_normal =
                                         // if there is a continuation, then use average of current and next
                                         // normals:
                                         (isnt_last)
                                         ? (branch.segments[ii].vec.Normalized() + branch.segments[ii + 1].vec.Normalized()).Normalized()
                                         : branch.segments[ii].vec.Normalized();
                l += segment.vec.Length();
                continue_segment(dt_portion.index_data,
                                 dt_portion.vertex_data,
                                 prev_root_pos + segment.vec,
                                 segment.endRadius,
                                 ch.circle_resolution,
                                 continue_normal,
                                 l,
                                 _bbTrunc);
                prev_root_pos = prev_root_pos + segment.vec;
            }

            if (po.needAlloc()) {
                _branchDataIndexIndexes.emplace_back(dt.size(), 0);
            } else {
                _branchDataIndexIndexes.emplace_back(dt.size() - 1,
                                                     dt.back().index_data.size());
            }
        }
    }

}

void TreeGenerator::regenerate () {
    using namespace detail;

    _clearData();
    generate_tree_data(tree_data, _treeCrownCenter, *_cfg, _mt);


    generate_leaves_data(leaf_data, tree_data, *_cfg, _mt, _treeCrownCenter);

    generate_tree_geometry(tree_data,
                           *_cfg,
                           _branchDataIndexIndexes,
                           dt,
                           dt_portion,
                           _bbTrunc);

    generate_leaves_geometry(dt_leaf,
                             dt_lf_portion,
                             _mt_lf,
                             _bbLeaves,
                             leaf_data,
                             *_cfg,
                             _treeCrownCenter,
                             _customLeavesModel);
    auto op = _cfg->wireframe ? Urho3D::LINE_LIST : Urho3D::TRIANGLE_LIST;


    std::vector<UrhoBits::GeomDataTuple> trunc_geoms;
    for (const auto &el : dt) {
        trunc_geoms.push_back(create_geometry(_context,
                                              el.index_data,
                                              el.vertex_data,
                                              op));
    }

    _model = create_model(_context, trunc_geoms, _bbTrunc);


    std::vector<UrhoBits::GeomDataTuple> leaf_geoms;
    // first element having 0 vertices means array is effectively empty
    if (dt_leaf.size() && dt_leaf[0].index_data.size() > 0) {
        for (const auto &el : dt_leaf) {
            leaf_geoms.push_back(create_geometry(_context,
                                                 el.index_data,
                                                 el.vertex_data));
        }
    }

    _leafModel = create_model(_context, leaf_geoms, _bbLeaves);

    if (!_debugRender) {
        return;
    }

    if (!_sceneNode) {
        _sceneNode = _scene->CreateChild("a tree");
        _sceneNode->SetPosition(Vector3(0, 0, 0));
        modl =
            _sceneNode->CreateComponent<StaticModel>();
        leaf_modl =
            _sceneNode->CreateComponent<StaticModel>();
        leaf_modl->SetCastShadows(true);

        modl->SetCastShadows(true);
    }

    modl->SetModel(_model);
    modl->SetMaterial(_cache->GetResource<Material>("Tree/Bark.xml"));
    leaf_modl->SetModel(_leafModel);
    auto leaf_mat = (_cfg->leaves_shape == TreeConfig::LeafShape::OBJECT)
        ? _customLeavesMaterial
        : _cache->GetResource<Material>("Tree/Leaf.xml");
    leaf_modl->SetMaterial(leaf_mat);
}

} // namespace UrhoBits
