// Copyright 2015-2022 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace UrhoBits {

struct Branch {
    struct Segment {
        Urho3D::Vector3 vec;
        float endRadius;
    };

    struct ChildrenData {
        float position; // distance from parent branch root to the child
                        // position (measured along the curved branch, not the
                        // shortest line)
        size_t index; // index of the child branch in the resulting branches
                      // array
    };

    Urho3D::Vector3 rootPos;
    float rootRadius;
    float length;
    float lengthMultiplier; // shows by how much this branch was reduced
                            // compared to base level length
    float parentRadius;
    std::vector<Segment> segments;
    std::vector<ChildrenData> children;
    size_t parent;

    // WARNING segments[0] branch radius is 1, root branch is 0
    float getNthBranchRadius (const size_t pNum) const {
        // TODO3: cleanup
        if (pNum == 0) {
            return rootRadius;
        }

        if (pNum <= segments.size()) {
            return segments[pNum-1].endRadius;
        }
        return segments.back().endRadius;
    }
};

struct Leaf {
    Urho3D::Vector3 pos;
    Urho3D::Vector3 vec;
    Urho3D::Vector3 norm;
};


// branches within container have pointers to child elements, therefore
// container should keep elements at the same adress throughout it's life
using BranchesContainer = std::deque<Branch>;

class BranchesLevel {
public:
    friend class Branches;

    // TODO enforce (or fix?) failure when adding to a level when there are
    // elements past

    Branch& push_back (Branch&& pBranch) {
        _data.push_back(pBranch);
        ++_size;
        return _data.back();
    }

    size_t getCurrentIndex () const {
        return _data.size();
    }

    BranchesContainer::iterator begin () const {
        return _data.begin() + _begin;
    }

    BranchesContainer::iterator end () const {
        return _data.begin() + (_begin + _size);
    }

    size_t size () const {
        return _size;
    }

private:

    BranchesLevel (BranchesContainer &pData,
                   size_t pLevel,
                   size_t pBegin,
                   size_t pSize = size_t(-1))
        : _data (pData),
          _level (pLevel),
          _begin (pBegin),
          _size (pSize) {}
    BranchesLevel (const BranchesLevel&) = default;

    BranchesContainer &_data;
    const size_t _level;
    const size_t _begin;
    size_t _size = -1;
};

class Branches {
public:
    const Branch &operator [] (size_t pNum) const {
        return _data[pNum];
    }
    Branch &operator [] (size_t pNum) {
        return _data[pNum];
    }

    BranchesLevel getLevel (size_t pLevel) {
        if (pLevel < _levelsIndexes.size()) {
            return BranchesLevel(_data, pLevel, _levelsIndexes[pLevel], _calcLevelSize(pLevel));
        } else {
            throw std::runtime_error("Branches::getLevel(): no such level");
        }
    }

    const BranchesLevel getLevel (size_t pLevel) const {
        if (pLevel < _levelsIndexes.size()) {
            return BranchesLevel(_data, pLevel, _levelsIndexes[pLevel], _calcLevelSize(pLevel));
        } else {
            throw std::runtime_error("Branches::getLevel(): no such level");
        }
    }

    BranchesLevel addLevel () {
        _levelsIndexes.push_back(_data.size());
        return BranchesLevel(_data,
                             _levelsIndexes.size() - 1,
                             _levelsIndexes.back());
    }

    size_t getNumLevels () const {
        return _levelsIndexes.size();
    }

    size_t size () const {
        return _data.size();
    }

    void clear () {
        _data.clear();
        _levelsIndexes.clear();
    }
private:
    size_t _calcLevelSize (size_t pLevel) const {
        if (pLevel + 1 < _levelsIndexes.size()) {
            return _levelsIndexes[pLevel + 1] - _levelsIndexes[pLevel];
        } else {
            return _data.size() - _levelsIndexes[pLevel];
        }
    }

    mutable BranchesContainer _data;

    // each element defines starting position of each level within _data
    std::vector<size_t> _levelsIndexes;
};

} // namespace UrhoBits
