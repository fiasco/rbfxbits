// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Urho3D/ThirdParty/EASTL/vector.h>

#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Graphics/GraphicsDefs.h>

#include <UrhoBits/GeometryGenerator/GeometryGenerator.hpp>

namespace UrhoBits {
using namespace Urho3D;
struct TrunkVertexElement {
	Vector3 coord;
	Vector3 normal;
	Vector2 texcoord1;
	Vector2 texcoord2;
	UrhoworldsColorVertexElement color = {0, 0, 0, 0};
	static ea::vector<VertexElement>
	getVertexDescription() {
		ea::vector<VertexElement> descr;
		descr.push_back(VertexElement(TYPE_VECTOR3,
									  SEM_POSITION));
		descr.push_back(VertexElement(TYPE_VECTOR3,
									  SEM_NORMAL));
		descr.push_back(VertexElement(TYPE_VECTOR2,
									  SEM_TEXCOORD));
		descr.push_back(VertexElement(TYPE_VECTOR2,
									  SEM_TEXCOORD,
									  2));
		descr.push_back(VertexElement(TYPE_UBYTE4,
									  SEM_COLOR));
		return descr;
	}
};
	
} // namespace UrhoBits