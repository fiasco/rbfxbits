#include "VcppBits/contrib/catch2/catch.hpp"

#include "Utils.hpp"

#include <iostream>

using namespace UrhoBits;

TEST_CASE("TreeGeneratorTests", "[Meah]") {
    REQUIRE(2.f / 2.f == Approx(1.f));

    struct test_data_struct {
        size_t num_src;
        size_t num_dst;
        std::vector<Branch::ChildrenData> children;
        bool fix_len;
    };

    std::vector<test_data_struct> dt = {
                                        { 20, 34, {}, false },
                                        { 19, 33, {}, true },
                                        { 5, 4, {{0.5f, 0}}, false },
    };


    for (const auto &el : dt) {
        NurbsIter iter{el.num_src, el.num_dst, el.children, el.fix_len};
        std::vector<float> results;
        float prev = 0.f;
        for (;!iter.isDone(); iter.advance()) {
            float i = iter.getCurrent();
            results.push_back(i);
            prev = iter.getCurrent();
        }

        REQUIRE(results[0] == Approx(0.f));
        REQUIRE(results.back() == Approx(float(el.num_src)));

        size_t i = 0;
        for (auto each_result : results) {
            REQUIRE(each_result >= Approx(0.f));
            REQUIRE(each_result <= Approx(float(el.num_src)));

            float expected = float(el.num_src) / float(el.num_dst) * float(i);
            REQUIRE(each_result == Approx(expected));
            ++i;
        }
    }
}


TEST_CASE("TreeGeneratorTests2", "[Meah2]") {
    REQUIRE(2.f / 2.f == Approx(1.f));

    struct test_data_struct {
        size_t num_src;
        size_t num_dst;
        std::vector<Branch::ChildrenData> children;
        bool fix_len;
        std::vector<float> results;
    };

    std::vector<test_data_struct> dt = {
        { 5, 4, {{0.5f, 0}}, true, {0.f, 1.25f, 2.5f, 3.75f, 5.f} },
        { 5, 4, {}, true,         {0.f, 1.25f, 2.5f, 3.75f, 5.f} },
        { 5, 4, {{0.6f, 0}}, true, {0.f, 1.f, 2.f, 3.f, 4.f, 5.f} },
        { 5, 3, {{0.f, 0}}, true, {0.f, 5.f/3.f, 5.f/3.f*2.f, 5.f} },

        { 5, 7, {{0.6f, 0}, {0.7456f, 1}}, true,
          { 0.f, 0.6f, 1.2f, 1.8f, 2.4f, 3.f, 0.7456f * 5.f, (0.7456f * 5.f + 5.f)/2.f, 5.f } },
    };


    for (const auto &el : dt) {
        NurbsIter iter{el.num_src, el.num_dst, el.children, el.fix_len};
        std::vector<float> results;
        float prev = 0.f;
        for (;!iter.isDone(); iter.advance()) {
            float i = iter.getCurrent();
            results.push_back(i);
            prev = iter.getCurrent();
        }

        // basic check
        REQUIRE(results[0] == Approx(0.f));
        REQUIRE(results.back() == Approx(float(el.num_src)));

        size_t i = 0;
        for (auto each_result : results) {
            REQUIRE(each_result >= Approx(0.f));
            REQUIRE(each_result <= Approx(float(el.num_src)));


            REQUIRE(each_result == Approx(el.results[i]));

            ++i;
        }
    }
}


