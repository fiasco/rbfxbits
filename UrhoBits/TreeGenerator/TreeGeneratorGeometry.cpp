// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#include "TreeGeneratorGeometry.hpp"


#include <Urho3D/Math/Quaternion.h>
#include <Urho3D/Math/Ray.h>

#include "VcppBits/MathUtils/MathUtils.hpp"


namespace UrhoBits {
	
using namespace Urho3D;
using namespace VcppBits;
using namespace VcppBits::MathUtils;

void create_circle_vertices(
		SimpleVector<TrunkVertexElement> &vertices,
		const Vector3 &center,
		const Vector3 &normal,
		float radius,
		int resolution,
		float l /* distance to branch root */,
		BoundingBox &pBounds,
		std::function<void(TrunkVertexElement&)>adjust_func) {

    const Quaternion orientation (Vector3::UP, normal);

    Vector3 p = orientation * Vector3(radius, 0, 0) + center;

    const float incr = 1.f / ((float)resolution);
    for (int i = 0; i <= resolution; ++i) {
        vertices.push_back({
                p,
                p - center,
                { incr * (float)i, l },
                { incr * (float)i, l },
                { 0, 0, 0, 0 }
            });
        adjust_func(vertices.back());

        const float angle = (float)(i+1) / ((float)resolution) * 360.0f;
        // TODO: Try using precomputed array of sin() and cos()?
        const Vector3 v(radius * Cos(angle), 0, radius * Sin(angle));
        p = orientation * v + center;

        pBounds.Merge(p);
    }
}



void create_weilded_circle_vertices(
        const SimpleVector<UrhoworldsIndexElement> &parent_indexes,
        const SimpleVector<TrunkVertexElement> &parent_vertices,
        size_t index_pos,
        size_t index_num,
        SimpleVector<TrunkVertexElement> &vertices,
        const Vector3 &center,
        const Vector3 &normal,
        float radius,
        int resolution,
        float l /* distance to branch root */,
        BoundingBox &pBounds,
        float slide_forward,
        unsigned char red_vertex_color) {

    float firstX = -2.0; // intentionally out of range of normal ray hit

    auto process_ray_hit =
        [&] (TrunkVertexElement& pPoint) {
            // TODO calculate ray length as some function of parent radius
            Ray ray (pPoint.coord + normal * 5 * radius, -normal);
            Vector3 parent_normal;
            float distance = ray.HitDistance(parent_vertices.data(),
                                             sizeof(TrunkVertexElement),
                                             parent_indexes.data(),
                                             2, /* size of each index element*/
                                             index_pos, // index
                                             index_num * 3,
                                             &(parent_normal),
                                             &(pPoint.texcoord2),
                                             24); // TODO shitty hardcode, nontrivial to fix :/
            // TODO3: if ray didn't hit, but others on the circle did, our UV
            // coordinate is messed up now
            if (distance == distance && distance < 100.f) {
                pPoint.coord = ray.origin_ + -normal * distance;
            }
            if (parent_normal.DotProduct(pPoint.normal) > 0.f) {
                pPoint.normal = parent_normal; // TODO make it average
            }

            if (firstX > 0.f) {
                float dx_delta = pPoint.texcoord2.x_ - firstX;
                float dx_sign = sign(dx_delta);
                if (std::abs(dx_delta) > 5.0f / float(resolution)) {
                    pPoint.texcoord2.x_ -= dx_sign;
                }
            }

            pPoint.coord += normal.Normalized() * slide_forward;
            pPoint.color.r = red_vertex_color;
            if (firstX < 0.f) {
                firstX = pPoint.texcoord2.x_;
            }
        };
    create_circle_vertices(vertices,
                           center,
                           normal,
                           radius,
                           resolution,
                           l,
                           pBounds,
                           process_ray_hit);
}

void fill_segment_indexes (
        const unsigned int& starting_index,
        const int& resolution,
        VcppBits::SimpleVector<UrhoworldsIndexElement>& indexes)
{
    for (unsigned j = starting_index; j < (unsigned(resolution) + starting_index); ++j) {
        unsigned short point0_num = j;
        unsigned short point1_num = j + resolution + 1;
        unsigned short point2_num = j + 1;

        indexes.push_back({ point0_num, point1_num, point2_num });

        unsigned short point5_num = point1_num + 1;

        indexes.push_back({ point2_num, point1_num, point5_num });
    }
}

void create_weilded_segment (const SimpleVector<UrhoworldsIndexElement> &indexes0,
                             const SimpleVector<TrunkVertexElement> &vertices0,
                             size_t index_pos,
                             size_t index_num,
                             SimpleVector<UrhoworldsIndexElement> &indexes,
                             SimpleVector<TrunkVertexElement> &vertices,
                             const Vector3 &root,
                             const float root_radius,
                             const Vector3 &tip,
                             const float tip_radius,
                             const int resolution,
                             const Vector3 &end_normal,
                             const float l /*distance to branch root, UV coord that is*/,
                             BoundingBox &pBounds,
                             const bool weild) {
    unsigned starting_index = unsigned(vertices.size());

    Vector3 normal = (tip - root).Normalized();

    // circle 1

    if (weild) {
        create_weilded_circle_vertices(indexes0,
                                       vertices0,
                                       index_pos,
                                       index_num,
                                       vertices,
                                       root,
                                       normal,
                                       root_radius * 1.13,
                                       resolution,
                                       l - (tip - root).Length() / 10.f,
                                       pBounds,
                                       0.f,
                                       weild ? 255 : 0);
        // circle 2
        // not a problem to fill indexes before vertices
        fill_segment_indexes(starting_index, resolution, indexes);
        starting_index = vertices.size();
        create_weilded_circle_vertices (indexes0,
                                        vertices0,
                                        index_pos,
                                        index_num,
                                        vertices,
                                        root,
                                        normal,
                                        root_radius,
                                        resolution,
                                        l,
                                        pBounds,
                                        .02);
        //starting_index += resolution;
    } else {
        create_circle_vertices(vertices,
                               root,
                               normal,
                               root_radius,
                               resolution,
                               l - (tip - root).Length() / 10.f,
                               pBounds);
    }

    // circle 3
    create_circle_vertices (vertices,
                            tip,
                            end_normal,
                            tip_radius,
                            resolution,
                            l + (tip - root).Length(),
                            pBounds);

    fill_segment_indexes(starting_index, resolution, indexes);
}

void create_segment (SimpleVector<UrhoworldsIndexElement> &indexes,
                     SimpleVector<TrunkVertexElement> &vertices,
                     const Vector3 &root,
                     const float root_radius,
                     const Vector3 &tip,
                     const float tip_radius,
                     const int resolution,
                     const Vector3 &end_normal,
                     const float l /*distance to branch root, UV coord that is*/,
                     BoundingBox &pBounds) {
    const unsigned starting_index = unsigned(vertices.size());

    Vector3 normal = (tip - root).Normalized();

    // circle 1
    create_circle_vertices(vertices,
                           root,
                           normal,
                           root_radius,
                           resolution,
                           l,
                           pBounds);

    // circle 2
    create_circle_vertices(vertices,
                           tip,
                           end_normal,
                           tip_radius,
                           resolution,
                           l + (tip - root).Length(),
                           pBounds);

    fill_segment_indexes(starting_index, resolution, indexes);
}

void continue_segment (SimpleVector<UrhoworldsIndexElement> &indexes,
                       SimpleVector<TrunkVertexElement> &vertices,
                       const Vector3 &tip,
                       const float tip_radius,
                       const int resolution,
                       const Vector3 &end_normal,
                       const float l /*distance to branch root*/,
                       BoundingBox &pBounds) {
    const unsigned starting_index = unsigned(vertices.size()) - unsigned(resolution) - 1;

    // circle 2
    create_circle_vertices
        (vertices, tip, end_normal, tip_radius, resolution, l, pBounds);

    fill_segment_indexes(starting_index, resolution, indexes);
}

void create_diamond_leaf (SimpleVector<UrhoworldsIndexElement> &indexes,
                          SimpleVector<UrhoworldsTexturedTangentVertexElement> &vertices,
                          const Vector3 &lf_pos,
                          const Vector3 &lf_vec,
                          const Vector3 &lf_norm,
                          const Vector3 &normal,
                          Vector2 pTexDivider,
                          Vector2 pTexOffset,
                          BoundingBox &pBounds) {
    // root tri
    const auto sideways_vec = lf_vec.CrossProduct(lf_norm);

    const auto leaf_len = lf_vec.Length();
    const auto leaf_volume = leaf_len * 0.2f;

    // TODO4: stupid way to protect ourselves from overflow?
    const unsigned short i =
        (unsigned short) std::min(
            vertices.size(),
            size_t(std::numeric_limits<unsigned short>::max() - 3));

    // root vert
    vertices.push_back({ lf_pos,
                         normal,
                         Vector4{}, // TODO proper tangent
                         Vector2{0.5f, .0f} / pTexDivider
                         + pTexOffset});
    pBounds.Merge(lf_pos);
    auto v2 = lf_pos + sideways_vec /3
        + lf_vec * 0.3f + lf_norm * leaf_volume;
    auto v3 = lf_pos - sideways_vec / 3
        + lf_vec * 0.3f + lf_norm * leaf_volume;

    const auto v2_tex = Vector2(1.f, 1.f/3.f) / pTexDivider + pTexOffset;
    const auto v3_tex = Vector2(0.f, 1/3.f) / pTexDivider + pTexOffset;

    vertices.push_back({ v2, normal, Vector4{}, v2_tex });
    vertices.push_back({ v3, normal, Vector4{}, v3_tex });
    pBounds.Merge(v2);
    pBounds.Merge(v3);

    auto v4 = lf_pos + lf_vec;

    vertices.push_back({ v4,
                         normal,
                         Vector4{},
                         Vector2(0.5f, 1.f) / pTexDivider + pTexOffset  });
    pBounds.Merge(v4);


    indexes.push_back({ i,
                        static_cast<unsigned short>(i + 3),
                        static_cast<unsigned short>(i + 1)});
    indexes.push_back({ static_cast<unsigned short>(i),
                        static_cast<unsigned short>(i + 2),
                        static_cast<unsigned short>(i + 3)});
}


void create_square_leaf (SimpleVector<UrhoworldsIndexElement> &indexes,
                         SimpleVector<UrhoworldsTexturedTangentVertexElement> &vertices,
                         const Vector3 &lf_pos,
                         const Vector3 &lf_vec,
                         const Vector3 &lf_norm,
                         const Vector3 &normal,
                         Vector2 pTexDivider,
                         Vector2 pTexOffset,
                         BoundingBox &pBounds) {
    // root tri
    const auto sideways_vec = lf_vec.CrossProduct(lf_norm);

    // TODO4: stupid way to protect ourselves from overflow?
    const unsigned short i =
        (unsigned short) std::min(
            vertices.size(),
            size_t(std::numeric_limits<unsigned short>::max() - 3));

    const float leaf_uv_coord_incr_x = 1.f / float(pTexDivider.x_);
    const float leaf_uv_coord_incr_y = 1.f / float(pTexDivider.y_);

    // root vert
    auto v1 = lf_pos + sideways_vec * 0.5f;
    auto v1_tex = Vector2{} + pTexOffset;
    auto v2 = lf_pos - sideways_vec * 0.5f;
    auto v2_tex = Vector2(0.f + leaf_uv_coord_incr_x, 0.f) + pTexOffset;


    vertices.push_back({ v1, normal, Vector4{}, v1_tex });
    vertices.push_back({ v2, normal, Vector4{}, v2_tex });

    vertices.push_back({ v1 + lf_vec, normal, Vector4{}, v1_tex + Vector2(0.f, leaf_uv_coord_incr_y) });
    vertices.push_back({ v2 + lf_vec, normal, Vector4{}, v2_tex + Vector2(0.f, leaf_uv_coord_incr_y) });
    pBounds.Merge(v1);
    pBounds.Merge(v2);
    pBounds.Merge(v1 + lf_vec);
    pBounds.Merge(v2 + lf_vec);


    indexes.push_back({ i,
                        static_cast<unsigned short>(i + 1),
                        static_cast<unsigned short>(i + 2)});
    indexes.push_back({ static_cast<unsigned short>(i + 1),
                        static_cast<unsigned short>(i + 3),
                        static_cast<unsigned short>(i + 2)});
}


} // namespace UrhoBits
