// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

// TODO cleanup redundant includes
#pragma once

#include <vector>
#include <utility>
#include <deque>
#include <random>

#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/IO/FileSystem.h>

#include <UrhoBits/GeometryGenerator/GeometryGenerator.hpp>

#include "TreeConfig.hpp"
#include "TrunkVertexElement.hpp"
#include "TreePrimitives.hpp"


namespace Urho3D {
class Context;
class Scene;
class ResourceCache;
class Model;
class Material;
class StaticModel;
}


namespace UrhoBits {

struct TrunkDataFormat {
    SimpleVector<UrhoworldsIndexElement> index_data;
    SimpleVector<TrunkVertexElement> vertex_data;
};

struct LeavesDataFormat {
    SimpleVector<UrhoworldsIndexElement> index_data;
    SimpleVector<UrhoworldsTexturedTangentVertexElement> vertex_data;
};

struct TrunkVertexElement;

template <typename DataFormat>
class PortionedDataAppender {
public:
    PortionedDataAppender (std::deque<DataFormat> & pDest, DataFormat &pSrc)
        : _dest (pDest),
          _src (pSrc) {}
    ~PortionedDataAppender () {
        // move all src data to the destination if possible
        if (needAlloc()) {
            _dest.push_back(DataFormat{});
        } else {
        }
        using IndexDataType = decltype(_src.index_data[0].v1);
        IndexDataType index_offset =
            static_cast<IndexDataType>(_dest.back().vertex_data.size());

        for (auto &el: _src.index_data) {
            _dest.back().index_data.push_back(
                { IndexDataType(el.v1 + index_offset),
                  IndexDataType(el.v2 + index_offset),
                  IndexDataType(el.v3 + index_offset) });
        }
        for (auto &el: _src.vertex_data) {
            _dest.back().vertex_data.push_back(el);
        }
        _src.vertex_data.resize(0);
        _src.index_data.resize(0);
    }

    bool needAlloc () const {
        return _dest.back().vertex_data.size() + _src.vertex_data.size()
            >= std::numeric_limits<uint16_t>::max();
    }

    std::deque<DataFormat> &_dest;
    DataFormat &_src;
};


class TreeGenerator {
public:
    TreeGenerator ();


    void init (Urho3D::Scene* pScene, const bool debugRender);

    TreeConfig * getConfig () { return _cfg; }
    void setConfig (TreeConfig* pCfg) { _cfg = pCfg; }

    void setCustomLeavesModel (Urho3D::Model *pModel) {
        _customLeavesModel = pModel;
    }
    void setCustomLeavesMaterial (Urho3D::Material *pMaterial) {
        _customLeavesMaterial = pMaterial;
    }

    std::pair<Urho3D::SharedPtr<Urho3D::Model>,
              Urho3D::SharedPtr<Urho3D::Model>> getModels () {
        return { _model, _leafModel };
    }

    bool isInitialized () const { return _isInitialized; }

    Vector3 getBoundingBoxCenter () const {
        return (_bbTrunc.Center() + _bbLeaves.Center()) / 2.0f;
    }

    size_t getNumLeaves () { return leaf_data.size(); }

    void regenerate();

private:
    void _clearData ();

    std::tuple<Urho3D::Vector3, Urho3D::Vector3, float>
    getParentData (const size_t pChildIndex, const float pPosOffset) const;

    Urho3D::Node *_sceneNode = nullptr;
    Urho3D::StaticModel *modl;
    Urho3D::StaticModel *leaf_modl;

    Urho3D::SharedPtr<Urho3D::Model> _model;
    Urho3D::SharedPtr<Urho3D::Model> _leafModel;
    Branches tree_data;
    std::deque<Leaf> leaf_data;

    std::mt19937 _mt;
    std::mt19937 _mt_lf;

    Urho3D::Context *_context = nullptr;
    Urho3D::Scene* _scene = nullptr;
    Urho3D::ResourceCache *_cache = nullptr;

    TreeConfig *_cfg = nullptr;
    Urho3D::Model *_customLeavesModel = nullptr;
    Urho3D::Material *_customLeavesMaterial = nullptr;

    Urho3D::Vector3 _treeCrownCenter;

    Urho3D::BoundingBox _bbLeaves;
    Urho3D::BoundingBox _bbTrunc;

    bool _debugRender = false;
    bool _isInitialized = false;


    std::deque<TrunkDataFormat> dt;
    std::deque<LeavesDataFormat> dt_leaf;

    // <position in dt, position in dt.index_data>
    std::vector<std::pair<size_t, size_t>> _branchDataIndexIndexes;

    TrunkDataFormat dt_portion;
    LeavesDataFormat dt_lf_portion;

};


std::tuple<Urho3D::Model*, Urho3D::Model*, Urho3D::Material*, Urho3D::Material*>
buildTree (Urho3D::Node *pNode);

} // namespace UrhoBits
