// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.


#pragma once

#include <functional>

#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Math/BoundingBox.h>

#include <VcppBits/SimpleVector/SimpleVector.hpp>

#include "TrunkVertexElement.hpp"

namespace UrhoBits {

void create_circle_vertices (SimpleVector<TrunkVertexElement> &vertices,
		const Vector3 &center,
		const Vector3 &normal,
		float radius,
		int resolution,
		float l /* distance to branch root */,
		BoundingBox &pBounds,
		std::function<void(TrunkVertexElement&)>adjust_func =
			[](TrunkVertexElement& el){
				el.normal = el.normal.Normalized();
			});
			 
 void create_weilded_circle_vertices (
        const SimpleVector<UrhoworldsIndexElement> &parent_indexes,
        const SimpleVector<TrunkVertexElement> &parent_vertices,
        size_t index_pos,
        size_t index_num,
        SimpleVector<TrunkVertexElement> &vertices,
        const Vector3 &center,
        const Vector3 &normal,
        float radius,
        int resolution,
        float l /* distance to branch root */,
        BoundingBox &pBounds,
        float slide_forward = 0.f, // TODO make a separate function that will just slide previously generated points
        unsigned char red_vertex_color = 0);
        
void create_segment (SimpleVector<UrhoworldsIndexElement> &indexes,
                     SimpleVector<TrunkVertexElement> &vertices,
                     const Vector3 &root,
                     const float root_radius,
                     const Vector3 &tip,
                     const float tip_radius,
                     const int resolution,
                     const Vector3 &end_normal,
                     const float l /*distance to branch root, UV coord that is*/,
                     BoundingBox &pBounds);
                     
void create_weilded_segment (const SimpleVector<UrhoworldsIndexElement> &indexes0,
                             const SimpleVector<TrunkVertexElement> &vertices0,
                             size_t index_pos,
                             size_t index_num,
                             SimpleVector<UrhoworldsIndexElement> &indexes,
                             SimpleVector<TrunkVertexElement> &vertices,
                             const Vector3 &root,
                             const float root_radius,
                             const Vector3 &tip,
                             const float tip_radius,
                             const int resolution,
                             const Vector3 &end_normal,
                             const float l /*distance to branch root, UV coord that is*/,
                             BoundingBox &pBounds,
                             const bool weild);
        
void continue_segment (SimpleVector<UrhoworldsIndexElement> &indexes,
                       SimpleVector<TrunkVertexElement> &vertices,
                       const Vector3 &tip,
                       const float tip_radius,
                       const int resolution,
                       const Vector3 &end_normal,
                       const float l /*distance to branch root*/,
                       BoundingBox &pBounds);
        
void create_diamond_leaf (SimpleVector<UrhoworldsIndexElement> &indexes,
                          SimpleVector<UrhoworldsTexturedTangentVertexElement> &vertices,
                          const Vector3 &lf_pos,
                          const Vector3 &lf_vec,
                          const Vector3 &lf_norm,
                          const Vector3 &normal,
                          Vector2 pTexDivider,
                          Vector2 pTexOffset,
                          BoundingBox &pBounds);

void create_square_leaf (SimpleVector<UrhoworldsIndexElement> &indexes,
                         SimpleVector<UrhoworldsTexturedTangentVertexElement> &vertices,
                         const Vector3 &lf_pos,
                         const Vector3 &lf_vec,
                         const Vector3 &lf_norm,
                         const Vector3 &normal,
                         Vector2 pTexDivider,
                         Vector2 /*pTexOffset TODO implement me */,
                         BoundingBox &pBounds);

} // namespace UrhoBits
