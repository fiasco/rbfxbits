// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <array>

namespace UrhoBits {

namespace detail {
constexpr int MAX_LEAVES_LEVELS = 4;

} // namespace detail

struct TreeConfig {
    // global settings TODO
    int num_levels;
    int seed;
    float branch_curvature_threshold;
    float root_length;
    bool wireframe;
    float root_radius;
    int leaves_level;
    float lf_leaf_size;
    float leaves_normals_outwards;
    int leaves_per_meter; //TODO turn to float
    float leaves_offset;

    int leaves_atlas_res_x;
    int leaves_atlas_res_y;

    int norender_levels;

    enum class LeafShape : int { DIAMOND, SQUARE, POINT_STUB, OBJECT };
    LeafShape leaves_shape;

    struct LevelSettings {
        float endpoint_outward_k;
        float endpoint_Zward_k;
        float endpoint_random_k;
        float endpoint_parent_k;
        float endpoint_crown_outward_k;
        float endpoint_influence;
        float radius_multi;
        float branching_radius_multiplier;
        int num_segments;
        float growth_dev;

        float length_distribution_equal;
        float length_distribution_circle;
        float length_distribution_cone;
        float length_distribution_flame;
        float length_distribution_random;

        float length_multiplier;

        float outward_k;
        float zward_k;
        float parent_k;
        float crown_outward_k;
        float crownpos_offset;
        float deviation_k;
        float acacia;
        float children_per_meter;
        int children_min;
        float offset;
        float offset_shortbranch_cancel;

        float branching_mirror_direction;

        float subdivisions_per_meter;

        int circle_resolution;

        bool fix_lengths; // TODO proper name
        bool weild; // TODO proper name
    };

    struct LeafSettings {
        float lf_direction_left_and_right_k;

	// TODO: rename to _direction_?
        float lf_endpoint_outward_k;
        float lf_endpoint_crown_outward_k;
        float lf_endpoint_Zward_k;
        float lf_endpoint_random_k;
        float lf_endpoint_parent_k;

        float lf_normal_parent_k;
        float lf_normal_outward_k;
        float lf_normal_upward_k;
    };

    std::array<LevelSettings, 6> levels;
    std::array<LeafSettings, UrhoBits::detail::MAX_LEAVES_LEVELS> leaves;
};

} // namespace UrhoBits
