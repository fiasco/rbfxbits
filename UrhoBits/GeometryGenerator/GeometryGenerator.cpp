// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#include <UrhoBits/GeometryGenerator/GeometryGenerator.hpp>

#include <Urho3D/Core/Context.h>
//#include <Urho3D/Core/Profiler.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/IO/Log.h>

#include <algorithm>
#include <random>
#include <stdexcept>
#include <tuple>
#include <utility>
#include <vector>

#include <VcppBits/MathUtils/MathUtils.hpp>

namespace UrhoBits {

using namespace Urho3D;

std::tuple<SharedPtr<Geometry>,
           SharedPtr<VertexBuffer>,
           SharedPtr<IndexBuffer>>
create_geometry (Context* pContext,
                 const GeometryConstructionData &data,
                 const Urho3D::PrimitiveType pType = Urho3D::TRIANGLE_LIST) {
  SharedPtr<Geometry> geom(new Geometry(pContext));
  SharedPtr<VertexBuffer> vb(new VertexBuffer(pContext, false));
  SharedPtr<IndexBuffer> ib(new IndexBuffer(pContext, false));

  // Shadowed buffer needed for raycasts to work, and so that data can be
  // automatically restored on device loss
  vb->SetShadowed(true);
  // We could use the "legacy" element bitmask to define elements for more
  // compact code, but let's demonstrate defining the vertex elements explicitly
  // to allow any element types and order

  //  PODVector<VertexElement> elements;
  vb->SetSize(data.getVertexDataSize(), data.getVertexDescription());
  vb->SetData(data.getVertexData());

  ib->SetShadowed(true);
  ib->SetSize(data.getIndexDataSize(), false);
  ib->SetData(data.getIndexData());

  geom->SetVertexBuffer(0, vb);
  geom->SetIndexBuffer(ib);
  geom->SetDrawRange(pType, 0, data.getIndexDataSize());

  return { geom, vb, ib};
}

SharedPtr<Model> create_2lod_plane (Context* pContext,
                                    const GeometryConstructionData &lod0,
                                    const GeometryConstructionData &lod1,
                                    const Vector3 &bounds0,
                                    const Vector3 &bounds1) {
  // bounds0: 0, -1, 0
  // bounds1: Vector3(static_cast<float>(res_x), 1, static_cast<float>(res_y)))

  SharedPtr<Model> fromScratchModel(new Model(pContext));
  ea::vector<SharedPtr<VertexBuffer> > vertexBuffers;
  ea::vector<SharedPtr<IndexBuffer> > indexBuffers;

  ea::vector<unsigned> morphRangeStarts;
  ea::vector<unsigned> morphRangeCounts;
  morphRangeStarts.push_back(0);
  morphRangeCounts.push_back(0);

  fromScratchModel->SetNumGeometries(1);
  fromScratchModel->SetNumGeometryLodLevels(0, 2);

  SharedPtr<Geometry> geom;
  SharedPtr<VertexBuffer> vb;
  SharedPtr<IndexBuffer> ib;

  std::tie(geom, vb, ib) = create_geometry(pContext, lod0);
  vertexBuffers.push_back(vb);
  indexBuffers.push_back(ib);
  geom->SetLodDistance(5.f);
  fromScratchModel->SetGeometry(0, 0, geom);

  std::tie(geom, vb, ib) = create_geometry(pContext, lod1);
  vertexBuffers.push_back(vb);
  indexBuffers.push_back(ib);
  geom->SetLodDistance(10.f);
  fromScratchModel->SetGeometry(0, 1, geom);

  fromScratchModel->SetBoundingBox(BoundingBox(bounds0, bounds1));
  fromScratchModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
  fromScratchModel->SetIndexBuffers(indexBuffers);

  return fromScratchModel;
}

SharedPtr<Model> create_1lod_plane (Context* pContext,
                                    const GeometryConstructionData &lod0,
                                    const Vector3 &bounds0,
                                    const Vector3 &bounds1,
                                    const Urho3D::PrimitiveType pType
                                        /* = Urho3D::TriangleList */) {
  SharedPtr<Model> fromScratchModel(new Model(pContext));
  ea::vector<SharedPtr<VertexBuffer> > vertexBuffers;
  ea::vector<SharedPtr<IndexBuffer> > indexBuffers;

  ea::vector<unsigned> morphRangeStarts;
  ea::vector<unsigned> morphRangeCounts;
  morphRangeStarts.push_back(0);
  morphRangeCounts.push_back(0);

  fromScratchModel->SetNumGeometries(1);
  fromScratchModel->SetNumGeometryLodLevels(0, 1);

  SharedPtr<Geometry> geom;
  SharedPtr<VertexBuffer> vb;
  SharedPtr<IndexBuffer> ib;

  std::tie(geom, vb, ib) = create_geometry(pContext, lod0, pType);
  vertexBuffers.push_back(vb);
  indexBuffers.push_back(ib);
  geom->SetLodDistance(5.f);
  fromScratchModel->SetGeometry(0, 0, geom);


  fromScratchModel->SetBoundingBox(BoundingBox(bounds0, bounds1));
  fromScratchModel->SetVertexBuffers(vertexBuffers, morphRangeStarts, morphRangeCounts);
  fromScratchModel->SetIndexBuffers(indexBuffers);

  return fromScratchModel;
}

} // namespace UrhoBits
