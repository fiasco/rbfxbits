// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include <memory>
#include <stdexcept>

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/GraphicsDefs.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/VertexBuffer.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/IO/Log.h>

#include <VcppBits/SimpleVector/SimpleVector.hpp>

namespace UrhoBits {

using VcppBits::SimpleVector;

struct UrhoworldsIndexElement {
  unsigned short v1, v2, v3;
};

struct UrhoworldsColorVertexElement {
  unsigned char r, g, b, a;
};


struct UrhoworldsLandscapeVertexElement {
  Urho3D::Vector3 coord;
  Urho3D::Vector3 normal;
  UrhoworldsColorVertexElement color;
  static ea::vector<Urho3D::VertexElement>
  getVertexDescription() {
      ea::vector<Urho3D::VertexElement> descr;
      descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                            Urho3D::SEM_POSITION));
      descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                            Urho3D::SEM_NORMAL));
      descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_UBYTE4,
                                            Urho3D::SEM_COLOR));

      return descr;
  }
};

struct UrhoworldsTexturedVertexElement {
    Urho3D::Vector3 coord;
    Urho3D::Vector3 normal;
    Urho3D::Vector2 texcoord;
    static ea::vector<Urho3D::VertexElement>
    getVertexDescription() {
        ea::vector<Urho3D::VertexElement> descr;
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                              Urho3D::SEM_POSITION));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                              Urho3D::SEM_NORMAL));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR2,
                                              Urho3D::SEM_TEXCOORD));
        return descr;
    }
};

struct UrhoworldsTexturedTangentVertexElement {
    Urho3D::Vector3 coord;
    Urho3D::Vector3 normal;
    Urho3D::Vector4 tangent;
    Urho3D::Vector2 texcoord;
    static ea::vector<Urho3D::VertexElement>
    getVertexDescription() {
        ea::vector<Urho3D::VertexElement> descr;
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                              Urho3D::SEM_POSITION));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                              Urho3D::SEM_NORMAL));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR4,
                                              Urho3D::SEM_TANGENT));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR2,
                                              Urho3D::SEM_TEXCOORD));
        return descr;
    }
};


struct UrhoworldsTexturedNormalMappedVertexElement {
    Urho3D::Vector3 coord;
    Urho3D::Vector3 normal;
    Urho3D::Vector3 tangent;
    Urho3D::Vector2 texcoord;
    static ea::vector<Urho3D::VertexElement>
    getVertexDescription() {
        ea::vector<Urho3D::VertexElement> descr;
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                              Urho3D::SEM_POSITION));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                              Urho3D::SEM_NORMAL));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3,
                                              Urho3D::SEM_TANGENT));
        descr.push_back(Urho3D::VertexElement(Urho3D::TYPE_VECTOR2,
                                              Urho3D::SEM_TEXCOORD));
        return descr;
    }
};


class GeometryConstructionData {
public:
    virtual ea::vector<Urho3D::VertexElement>
    getVertexDescription () const = 0;

    virtual void *getVertexData () const = 0;
    virtual unsigned getVertexDataSize () const = 0;

    virtual void *getIndexData () const = 0;
    virtual unsigned getIndexDataSize () const = 0;
};

template <typename VertexElementType>
struct UrhoworldsGeometryConstructionData {
    UrhoworldsGeometryConstructionData (Urho3D::Context *pContext)
        : index_buffer (pContext),
          vertex_buffer (pContext) {
    }
    ea::vector<Urho3D::VertexElement>
    getVertexDescription () const {
        return VertexElementType::getVertexDescription();
    }

    Urho3D::IndexBuffer index_buffer;
    Urho3D::VertexBuffer vertex_buffer;
};

// TODO4: remove? switch to UrhoworldsGeometryConstructionData
template <typename VertexElementType>
class UrhoworldsPlaneConstructionDataBase : public GeometryConstructionData {
public:
    ea::vector<Urho3D::VertexElement>
    getVertexDescription () const final override {
        return VertexElementType::getVertexDescription();
    }

    void *getVertexData () const final override { return vertex_data->data(); }
    unsigned getVertexDataSize () const override {
        return (unsigned) vertex_data->size();
    }

    void *getIndexData () const final override { return index_data->data(); }
    unsigned getIndexDataSize () const final override {
        constexpr size_t sz =
            sizeof(UrhoworldsIndexElement) / sizeof(UrhoworldsIndexElement::v1);
        return (unsigned) (index_data->size() * sz);
    }

    SimpleVector<UrhoworldsIndexElement> *index_data;
    SimpleVector<VertexElementType> *vertex_data;
};

template <typename VertexElementType>
class UrhoworldsPlaneConstructionData final
    : public UrhoworldsPlaneConstructionDataBase<VertexElementType> {
public:
    UrhoworldsPlaneConstructionData ()
        : index_data_storage
              (std::make_unique<SimpleVector<UrhoworldsIndexElement>>()),
          vertex_data_storage
              (std::make_unique<SimpleVector<VertexElementType>>()) {
        UrhoworldsPlaneConstructionDataBase<VertexElementType>::index_data
            = index_data_storage.get();
        UrhoworldsPlaneConstructionDataBase<VertexElementType>::vertex_data
            = vertex_data_storage.get();
    }

    void clear () {
        index_data_storage->resize(0);
        vertex_data_storage->resize(0);
    }

    std::unique_ptr<SimpleVector<UrhoworldsIndexElement>> index_data_storage;
    std::unique_ptr<SimpleVector<VertexElementType>> vertex_data_storage;
};


using GeomDataTuple = std::tuple<Urho3D::SharedPtr<Urho3D::Geometry>,
                                 Urho3D::SharedPtr<Urho3D::VertexBuffer>,
                                 Urho3D::SharedPtr<Urho3D::IndexBuffer>>;


inline Urho3D::SharedPtr<Urho3D::Model>
create_model (
    Urho3D::Context* pContext,
    std::vector<GeomDataTuple> &pGeometries,
    Urho3D::BoundingBox pBounds) {

    Urho3D::SharedPtr<Urho3D::Model> model (new Urho3D::Model(pContext));

    if (!pGeometries.size()) {
        return model;
    }

    model->SetNumGeometries(pGeometries.size());
    model->SetNumGeometryLodLevels(0, 1);

    ea::vector<Urho3D::SharedPtr<Urho3D::IndexBuffer>> index_buffer_ptrs;
    ea::vector<Urho3D::SharedPtr<Urho3D::VertexBuffer>> vertex_buffer_ptrs;

    for (size_t i = 0; i < pGeometries.size(); ++i) {
        model->SetGeometry(i, 0, std::get<0>(pGeometries[i]));
        vertex_buffer_ptrs.push_back(std::get<1>(pGeometries[i]));
        index_buffer_ptrs.push_back(std::get<2>(pGeometries[i]));
        continue;
    }
    model->SetBoundingBox(pBounds);

    ea::vector<unsigned> morphRangeStarts;
    ea::vector<unsigned> morphRangeCounts;
    morphRangeStarts.push_back(0);
    morphRangeCounts.push_back(0);

    model->SetIndexBuffers(index_buffer_ptrs);
    model->SetVertexBuffers(vertex_buffer_ptrs, morphRangeStarts, morphRangeCounts);

    return model;
}

template <typename VertexT>
std::tuple<Urho3D::SharedPtr<Urho3D::Geometry>,
           Urho3D::SharedPtr<Urho3D::VertexBuffer>,
           Urho3D::SharedPtr<Urho3D::IndexBuffer>>
create_geometry (
    Urho3D::Context* pContext,
    const SimpleVector<UrhoworldsIndexElement> &pIndexData,
    const SimpleVector<VertexT> &pVertexData,
    const Urho3D::PrimitiveType pType = Urho3D::TRIANGLE_LIST) {
    auto descr = VertexT::getVertexDescription();

    Urho3D::SharedPtr<Urho3D::Geometry> geom(new Urho3D::Geometry(pContext));
    Urho3D::SharedPtr<Urho3D::VertexBuffer> vb(new Urho3D::VertexBuffer(pContext, false));
    Urho3D::SharedPtr<Urho3D::IndexBuffer> ib(new Urho3D::IndexBuffer(pContext, false));

    vb->SetShadowed(true);

    vb->SetSize(pVertexData.size(), VertexT::getVertexDescription());
    vb->SetData(pVertexData.data());

    const auto index_data_size =
        pIndexData.size() * sizeof(UrhoworldsIndexElement) / sizeof(UrhoworldsIndexElement::v1);
    ib->SetShadowed(true);
    ib->SetSize(index_data_size, false);
    ib->SetData(pIndexData.data());

    geom->SetVertexBuffer(0, vb);
    geom->SetIndexBuffer(ib);
    geom->SetDrawRange(pType, 0, index_data_size);

    return { geom, vb, ib };
}


Urho3D::SharedPtr<Urho3D::Model>
create_2lod_plane (Urho3D::Context* pContext,
                   const GeometryConstructionData &lod0,
                   const GeometryConstructionData &lod1,
                   const Urho3D::Vector3 &bounds0,
                   const Urho3D::Vector3 &bounds1);

Urho3D::SharedPtr<Urho3D::Model>
create_1lod_plane (Urho3D::Context* pContext,
                   const GeometryConstructionData &lod0,
                   const Urho3D::Vector3 &bounds0,
                   const Urho3D::Vector3 &bounds1,
                   const Urho3D::PrimitiveType pType = Urho3D::TRIANGLE_LIST);
} // namespace UrhoBits
