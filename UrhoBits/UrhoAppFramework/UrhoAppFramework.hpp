// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Core/CoreEvents.h>

#include <Urho3D/Core/ProcessUtils.h> // GetPlatofrm()
#include <Urho3D/Engine/EngineDefs.h>
#include <Urho3D/IO/Log.h>

#include <UrhoBits/InputManager/InputManager.hpp>
#include <VcppBits/StateManager/StateManager.hpp>

#include <UrhoBits/UrhoAppFramework/IUpdatedState.hpp>

namespace UrhoBits {

class UrhoAppFramework : public Urho3D::Application,
                         public VcppBits::IStateChangesListener {
    URHO3D_OBJECT(UrhoAppFramework, Application);
public:
    explicit UrhoAppFramework (Urho3D::Context* context) :
        Application(context) {
        _updateableStates.reserve(8);
        _stateMgr.setListener(this);
    }

    // virtual methods for use
    virtual void setup () {}
    virtual void start () {}
    virtual void stop () {}
    virtual void update () {}
    virtual void endFrame () {}

    void processStateManagerEvent (const VcppBits::StateEventType pEventType,
                                   VcppBits::IState *pState) override {
        // we only need to be sure we have all loaded states in our
        // _updatableStates array

        // we check if it's actually running or not just by asking StateManager
        if (pEventType == VcppBits::StateEventType::LOAD) {
            IUpdatedState *updated = dynamic_cast<IUpdatedState*> (pState);
            if (updated) {
                _updateableStates.push_back(updated);
            }
        }

        if (pEventType == VcppBits::StateEventType::UNLOAD) {
            IUpdatedState *updated = dynamic_cast<IUpdatedState*> (pState);
            if (updated) {
                if (_updateableStates.back() != updated) {
                    throw 123; // TODO should never happen
                }
                _updateableStates.pop_back();
            }
        }

        URHO3D_LOGINFO((VcppBits::StateEventTypeUtils::toString(pEventType)
                        + " " + pState->getName()).c_str());

        if (pEventType == VcppBits::StateEventType::UNLOAD
            && _stateMgr.getFrozenState() == nullptr) {
            URHO3D_LOGINFO("Exiting");
            URHO3D_LOGINFO("...");
            engine_->Exit();
        }
    }

    void Setup () override {
        // Called before engine initialization. engineParameters_ member
        // variable can be modified here
        engineParameters_[Urho3D::EP_RESOURCE_PREFIX_PATHS] = ";.;../";

        engineParameters_[Urho3D::EP_WINDOW_TITLE] = GetTypeName();
        engineParameters_[Urho3D::EP_LOG_NAME]     =
         // GetSubsystem<FileSystem>()->GetAppPreferencesDir("urho3d", "logs") +
            GetTypeName() + ".log"; // TODO writing logs in current dir for now
            //"Humangen.log";
        engineParameters_[Urho3D::EP_FULL_SCREEN]  = false;
        engineParameters_[Urho3D::EP_HEADLESS]     = false;
        engineParameters_[Urho3D::EP_SOUND]        = false;
        engineParameters_[Urho3D::EP_FRAME_LIMITER] = false;
        engineParameters_[Urho3D::EP_WINDOW_RESIZABLE] = true;
        setup();
    }

    void Start () override {
        SubscribeToEvent(Urho3D::E_KEYDOWN,
                         URHO3D_HANDLER(UrhoAppFramework, handleKeyDown));

        SubscribeToEvent(Urho3D::E_UPDATE, URHO3D_HANDLER(UrhoAppFramework,
                                                          handleUpdate));

        SubscribeToEvent(Urho3D::E_ENDFRAME, URHO3D_HANDLER(UrhoAppFramework,
                                                            handleEndFrame));

        initMouse(Urho3D::MouseMode::MM_FREE);

        _inputMgr.start(); // TODO do we ACTUALLY need such method?
        start();
    }

    void Stop () override {
        // Perform optional cleanup after main loop has terminated
        _inputMgr.clearAll();
        _inputMgr.stop();
        stop();
    }

    void handleKeyDown (Urho3D::StringHash /*eventType*/,
                        Urho3D::VariantMap& eventData) {
        using namespace Urho3D::KeyDown;

        int key = eventData[P_KEY].GetInt();

        _inputMgr.processKey(key);
    }

    void handleUpdate (Urho3D::StringHash /*eventType*/,
                       Urho3D::VariantMap& eventData) {
        using namespace Urho3D::Update;

        // Take the frame time step, which is stored as a float
        float timeStep = eventData[P_TIMESTEP].GetFloat();

        for (auto *el : _updateableStates) {
            if (el == _stateMgr.currentState() || el->alwaysUpdate()) {
                el->update(timeStep);
            }
        }

        _stateMgr.update();

        update();
    }

    void handleEndFrame (Urho3D::StringHash /*eventType*/,
                         Urho3D::VariantMap& /*eventData*/) {
        //using namespace Urho3D::EndFrame;
        endFrame();
    }

    void initMouse (Urho3D::MouseMode mode) {
        _mouseMode = mode;

        Urho3D::Input* input = GetSubsystem<Urho3D::Input>();

        if (Urho3D::GetPlatform() != "Web") {
            if (_mouseMode == Urho3D::MM_FREE) {
                input->SetMouseVisible(true);
            }

            // Console* console = GetSubsystem<Console>();
            // if (_mouseMode != MM_ABSOLUTE) {
            //     input->SetMouseMode(_mouseMode);
            //     if (console && console->IsVisible()) {
            //         input->SetMouseMode(MM_ABSOLUTE, true);
            //     }
            // }
        } else {
            input->SetMouseVisible(true);
            SubscribeToEvent(Urho3D::E_MOUSEBUTTONDOWN,
                             URHO3D_HANDLER(UrhoAppFramework, handleMouseModeRequest));
            SubscribeToEvent(Urho3D::E_MOUSEMODECHANGED,
                             URHO3D_HANDLER(UrhoAppFramework, handleMouseModeChange));
        }
    }

    void handleMouseModeRequest(Urho3D::StringHash /*eventType*/,
                                Urho3D::VariantMap& /*eventData*/) {
        // Console* console = GetSubsystem<Console>();
        // if (console && console->IsVisible()) {
        //     return;
        // }
        Urho3D::Input* input = GetSubsystem<Urho3D::Input>();
        if (_mouseMode == Urho3D::MouseMode::MM_ABSOLUTE)
            input->SetMouseVisible(false);
        else if (_mouseMode == Urho3D::MouseMode::MM_FREE)
            input->SetMouseVisible(true);
        input->SetMouseMode(_mouseMode);
    }

    void handleMouseModeChange(Urho3D::StringHash /*eventType*/,
                               Urho3D::VariantMap& eventData) {
        Urho3D::Input* input = GetSubsystem<Urho3D::Input>();
        bool mouseLocked = eventData[Urho3D::MouseModeChanged::P_MOUSELOCKED].GetBool();
        input->SetMouseVisible(!mouseLocked);
    }


protected:
    InputManager _inputMgr;
    //    InputManagerAction action1 { &_inputMgr,
    //                                 Urho3D::KEY_ESCAPE,
    //                                 std::bind(&Urho3D::Engine::Exit, engine_) };

    VcppBits::StateManager _stateMgr;

    std::vector<IUpdatedState*> _updateableStates;

    Urho3D::MouseMode _mouseMode;
};

} // namespace UrhoBits
