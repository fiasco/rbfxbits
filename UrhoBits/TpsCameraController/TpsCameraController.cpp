// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#include "TpsCameraController.hpp"


#include <Urho3D/Math/StringHash.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Scene/Node.h>


namespace UrhoBits {

using namespace Urho3D;


TpsCameraController::TpsCameraController () {
}

void TpsCameraController::init (Urho3D::Node *pCameraNode) {
    _cameraNode = pCameraNode;
}

void TpsCameraController::moveCamera (const float /*timeStep*/,
                                      const IntVector2 &pMouseMove) {
    // sensitivity is degrees per pixel?
    auto yaw = _mouseSensitivity * pMouseMove.x_;
    auto pitch = _mouseSensitivity * pMouseMove.y_;
    pitch = Clamp(pitch, -90.0f, 90.0f); // clamping the rotation, not the
                                         // end result hehe

    _cameraNode->RotateAround(_point,
                              Quaternion(0, yaw, 0.0f),
                              TransformSpace::TS_WORLD);
    Quaternion ehh;
    ehh.FromAngleAxis(pitch, _cameraNode->GetWorldRight());
    _cameraNode->RotateAround(_point,
                              ehh,
                              TransformSpace::TS_WORLD);
}

void TpsCameraController::zoom (const float pZoomAmount) {
    auto curr_vec = _point - _cameraNode->GetWorldPosition();
    _cameraNode->Translate(curr_vec * (1.f - pZoomAmount), Urho3D::TS_WORLD);
}

} // namespace UrhoBits
