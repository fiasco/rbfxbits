// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#pragma once


#include <Urho3D/Core/Object.h>


namespace Urho3D {
class Context;
class Node;
}


namespace UrhoBits {

class TpsCameraController {
public:

    TpsCameraController ();
    void init (Urho3D::Node *pNode);

    void setPointOfInterest (const Urho3D::Vector3 &pPoint) {
        _point = pPoint;
    }

    void setMouseSensitivity (const float pSensitivity) {
        _mouseSensitivity = pSensitivity;
    }

    void update (const float pTimeStep,
                 const Urho3D::IntVector2 &pMouseMove) {
        moveCamera(pTimeStep, pMouseMove);
    }

    void zoom (const float pZoomAmount);

    bool isInitialized () {
        return _cameraNode;
    }
private:
    void moveCamera (const float pTimeStep,
                     const Urho3D::IntVector2 &pMouseMove);

    Urho3D::Node *_cameraNode = nullptr;

    Urho3D::Vector3 _point;

    //    float _yaw = 0.f; // no need to maintain? I think not
    //    float _pitch = 0.f;

    // settings
    float _mouseSensitivity = 0.1f;
};

} //namespace UrhoBits
