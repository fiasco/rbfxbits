
# parameter 1: name of the sub-target to add includes to
# parameter 2: INTERFACE, PUBLIC, PRIVATE scope...
macro(urhobits_include_toplevel_dir)

  get_filename_component(
    UrhoBits_TOPLEVEL_DIR
    "${CMAKE_CURRENT_SOURCE_DIR}/../.."
    ABSOLUTE
    )

  if (EXISTS "${UrhoBits_TOPLEVEL_DIR}")
    target_include_directories(
      "UrhoBits-${ARGV0}"
      ${ARGV1} "${UrhoBits_TOPLEVEL_DIR}")
  else ()
    message(FATAL_ERROR
      "\
${ARGV0} directory must be within UrhoBits/ for all ${ARGV0}'s includes to work\
properly. "
      )
  endif ()
endmacro()
