// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#include <UrhoBits/InputManager/InputManager.hpp>

namespace UrhoBits {

InputManagerAction::InputManagerAction (InputManager *pMgr,
                                        const int pKey,
                                        std::function<void()> pCallback)
    : _key (pKey),
      _callback (pCallback),
      _owner (pMgr){
    _owner->_insertAction(this);
}

InputManagerAction::~InputManagerAction () {
    _owner->_eraseAction (this);
}

} // namespace UrhoBits
