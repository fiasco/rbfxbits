// Copyright 2015-2020 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of UrhoBits.
//
// UrhoBits is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// UrhoBits is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with UrhoBits.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include <functional>
#include <algorithm>

namespace UrhoBits {

class InputManager;

class InputManagerAction {
public:
    explicit InputManagerAction (InputManager* pMgr,
                                 const int key,
                                 std::function<void()> pCallback);
    ~InputManagerAction ();

    int _key;

    std::function<void()> _callback;

private:
    InputManager* _owner;
};

class InputManager {
public:
    void processKey (const int pKey) {
        if (_active) {
            for (const auto &el : _actions) {
                if (el->_key == pKey) {
                    el->_callback();
                }
            }
        }
    }

    void clearAll () {
        _actions.clear();
    }

    void start () {
        _active = true;
    }

    void stop () {
        _active = false;
    }

    void _insertAction (InputManagerAction *pAction) {
        _actions.push_back(pAction);
    }

    void _eraseAction (InputManagerAction *pAction) {
        _actions.erase(std::remove(_actions.begin(), _actions.end(), pAction),
                       _actions.end());
    }

private:
    // TODO try different containers, and a fixed size container too (array or
    // vector)

    // TODO try to swap key and value (and use multimap obviously)
    std::vector<InputManagerAction*> _actions;
    bool _active = false;
};

} // namespace UrhoBits
