# RbfxBits

Some utilities and useful things for rbfx (Urho3D fork) game engine

Depends on https://gitlab.com/fiasco/vcppbits

...

There are no docs and not many reasons to publish this repo altogether, apart
from the fact that one of my current, and some of my next projects will likely
depend on this one.